---
title: Analisis Fresh
authors:
- Ashaeurizky D.
- Ali Akbar Septiandri
tags:
- knowledge
- utilisasi
- feedfund
created_at: 2020-06-02 00:00:00
updated_at: 2020-07-07 17:37:42.699571
tldr: 2020-06-11 melakukan pemindahan kode ke R
thumbnail: images/output_8_0.png
---

## Permasalahan


### Latar Belakang Masalah

Terkait MPP Project 3000, tim data menganalisis data kebutuhan dan potensi supply berdasarkan kategorinya. Beberapa potensi sudah diakomodasi di [Supply Dashboard](https://bi-dash.efishery.com/dashboard/119). Di luar hal tersebut, tim data berinisiatif untuk menganalisis data penjualan ke berbagai channel dalam beberapa tahun terakhir. Beberapa hal yang ingin dijawab oleh tim Fresh berdasarkan [diskusi ini](https://efishery.slack.com/archives/CBRUE6RDY/p1591594726027400) antara lain:
1. Masukan dalam membuat PO utk product makloon, misal bulan ini pesan bl size 4 glaze 40, 10 ton. Karena data penjualan atau proyeksi PO dari demand ke arah sana (baik data masa lalu atau contracted demand yg ada).
2. Dari PO itu akan turun spec komoditas ikan yg perlu kita tarik.
3. Bila memungkinkan juga by channelnya, karena gak semua product/harga bisa masuk ke channel tertentu. Misal harga dori yg di makloon di adib atau STP kemungkinan tidak bisa masuk di harga distributor, jadi perlu dialihkan ke MT atau retail.

### Batasan Masalah

1. Berhubung data penjualan terbatas, saat ini hanya bisa dilakukan sampai level size saja, belum termasuk glazing.
2. Beberapa channel dianggap tidak aktif lagi sehingga hanya difokuskan ke empat channel terbesar.

### Kamus Istilah

* Channel: Jalur penjualan, e.g. horeka, agen, distributor
* Lastmile

## Pengumpulan Data

### Sumber Datanya

[Data Prepared > Marketplace Transact Out](https://bi-dash.efishery.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo0NiwidHlwZSI6InF1ZXJ5IiwicXVlcnkiOnsic291cmNlLXRhYmxlIjo3OTR9fSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319)

### Pengertian Sumber Data

Data transaksi *lastmile* ke berbagai channel.

## Eksplorasi Data

### Analisis Pendahuluan


```python
#@title {display-mode: "form"}

library(tidyverse, warn.conflicts = FALSE)
theme_set(theme_minimal(base_size = 20))

options(repr.plot.width=20, repr.plot.height=8)
```

```python
#@title Memuat data {display-mode: "form"}

df <- read_csv('https://bi-dash.efishery.com/public/question/12fc198d-50ea-4d3c-851d-45985d882ac7.csv', col_type = cols()) %>%
    filter(!is.na(Channel)) %>%
    filter(!(Channel %in% c('Motoris Internal', 'Motoris Eksternal'))) %>%
    mutate(
        request_weight_ton = `Request Weight Kg` / 1e3
    )

big4 <- df %>%
    filter(Channel %in% c('Horeka','Distributor','Agen','Processing'))
```
Secara frekuensi penjualan, horeka menempati peringkat pertama, jauh mengungguli pilihan channel lainnya.


```python
#@title {display-mode: "form"}

ggplot(df) +
    geom_bar(aes(y = Channel))
```


![png](images/output_8_0.png)


Namun, secara total revenue, penjualan di horeka masih sangat bersaing dengan *processing* yang justru didominasi oleh beberapa jenis komoditas saja. Agen dan distributor menjadi channel lain yang memberikan revenue besar ke eFishery. Faktanya, 97% dari total revenue didapatkan dari empat channel tersebut.


```python
#@title {display-mode: "form"}

df %>%
    group_by(Channel) %>%
    summarize(
        total_revenue = sum(`Actual Total Price`, na.rm = TRUE),
        .groups = 'keep'
    ) %>%
    ggplot() +
    geom_col(
        aes(x = total_revenue, y = reorder(Channel, total_revenue))
    ) +
    labs(x = 'total revenue', y = '')
```


![png](images/output_10_0.png)


Meski secara revenue processing dan horeka hampir sama, ternyata secara tonnase processing mencapai lebih dari dua kali lipat dibandingkan horeka.


```python
#@title {display-mode: "form"}

df %>%
    group_by(Channel) %>%
    summarize(
        total_request = sum(request_weight_ton, na.rm = TRUE),
        .groups = 'keep'
    ) %>%
    ggplot() +
    geom_col(
        aes(x = total_request, y = reorder(Channel, total_request))
    ) +
    labs(x = 'total request (ton)', y = '')
```


![png](images/output_12_0.png)


Selain horeka, tiga channel besar lainnya tidak memiliki frekuensi yang cukup besar. Jadi, kita akan mengevaluasi permintaan berdasarkan waktu dari ketiga channel tersebut.


```python
#@title Permintaan per hari dari agen, distributor, dan processing {display-mode: "form"}

big4 %>%
    group_by(`Transact Out Date`, Channel, Commodity, `Actual Size`) %>%
    summarize(
        request_weight_ton,
        .groups = 'keep'
    ) %>%
    ungroup() %>%
    filter(Channel != 'Horeka') %>%
    complete(`Transact Out Date`, Channel, Commodity, `Actual Size`) %>%
    replace_na(list(request_weight_ton = 0)) %>%
    ggplot() +
    geom_col(aes(x = `Transact Out Date`, y = request_weight_ton, fill = Commodity)) +
    labs(y = 'request weight (ton)') +
    facet_grid(Channel ~ ., scales = 'free_y')
```


![png](images/output_14_0.png)


Komoditas yang menjadi permintaan dari
* agen adalah gurame,
* distributor adalah dori, dan
* processing adalah patin dan kepala dori.

Perlu diperhatikan bahwa belum ada permintaan dari distributor secara kuantitas tidak lagi sebanyak di bulan Desember 2019. Selain itu, permintaan patin di processing di bulan lalu (Mei 2020) justru mencapai puncaknya.


```python
#@title Median harga per komoditas per channel (dalam ribu rupiah) {display-mode: "form"}

df %>%
    mutate(commodity = recode(Commodity, lele = "Lele")) %>%
    filter(!is.na(commodity)) %>%
    group_by(Channel, commodity) %>%
    summarize(
        price = median(`Actual Price Per Kg`, na.rm = TRUE) / 1e3,
        price = round(price, digits = 0),
        .groups = 'drop_last'
    ) %>%
    filter(price != 0) %>%
    group_by(commodity) %>%
    mutate(
        rank = rank(price)
    ) %>%
    ggplot(aes(x = commodity, y = Channel)) +
    geom_tile(aes(fill = rank)) +
    geom_text(aes(label = price), color = 'white', size = 6) +
    theme(axis.text.x = element_text(angle = 90)) +
    guides(fill = FALSE) +
    coord_fixed()
```


![png](images/output_16_0.png)


### Horeka

Horeka perlu dianalisis secara spesifik karena secara perilakunya berbeda dari channel yang lainnya. Grafik berikut menunjukkan ukuran yang paling banyak dipesan (dalam ton) untuk setiap komoditas di 4 channel terbesar.


```python
#@title Ukuran yang paling banyak dipesan per channel per komoditas {display-mode: "form"}

big4 %>%
    filter(
        `Uom Size` == 'ekor/kg',
        Commodity != 'Barakuda'
    ) %>%
    group_by(Channel, Commodity, `Actual Size`) %>%
    summarize(
        total_request = sum(request_weight_ton),
        .groups = 'drop_last'
    ) %>%
    top_n(1, total_request) %>%
    ggplot(aes(x = Commodity, y = Channel)) +
    geom_tile(data = . %>% filter(Commodity != 'Udang Vanname'), aes(fill = `Actual Size`)) +
    geom_tile(data = . %>% filter(Commodity == 'Udang Vanname')) +
    geom_text(aes(label = `Actual Size`), color = 'white', size = 8) +
    theme(axis.text.x = element_text(angle = 90)) +
    guides(fill = FALSE) +
    coord_fixed()
```


![png](images/output_18_0.png)



```python
#@title {display-mode: "form"}

komoditas <- big4 %>%
    filter(
        !is.na(`Actual Size`),
        `Uom Size` == 'ekor/kg',
        `Commodity` != 'Udang Vanname'
    ) %>%
    group_by(Channel, Commodity, `Actual Size`) %>%
    summarize(
        jumlah_pesanan = n(),
        median_harga = median(`Actual Price Per Kg`, na.rm = TRUE),
        request_weight_ton = sum(request_weight_ton, na.rm = TRUE),
        total_revenue = sum(`Actual Total Price`, na.rm = TRUE),
        .groups = 'keep'
    )
```
Namun, frekuensi tidak selalu sebanding dengan jumlah tonnase yang dipesan maupun *revenue* yang didapatkan. Gurame dengan ukuran 3 dan 4 merupakan komoditas yang memberikan revenue paling besar dan tergolong cukup sering dipesan. Di sisi lain, dori dengan ukuran 5 lebih jarang dipesan, tetapi pesanannya dilakukan dalam jumlah besar dan menghasilkan potensi revenue yang besar.

Lele ukuran 10 secara tonnase cukup bayak dipesan (peringkat 3), tetapi revenue yang dihasilkan hanya setengahnya dari gurame. Jika kita mengejar efisiensi, mungkin lele bukan pilihan yang efisien. Namun, lele perlu dipertimbangkan sebagai diversifikasi penawaran.


```python
#@title {display-mode: "form"}

komoditas %>%
    filter(
        jumlah_pesanan >= 50,
        Channel == 'Horeka'
    ) %>%
    unite(sku, c(Channel, Commodity, `Actual Size`), sep = '-') %>%
    pivot_longer(-(sku)) %>%
    ggplot(aes(x = value, y = sku)) +
    geom_col() +
    geom_col(
        data = . %>% filter(sku %in% c('Horeka-Gurame-4', 'Horeka-Gurame-3')),
        fill = '#28B796'
    ) +
    geom_col(
        data = . %>% filter(sku %in% c('Horeka-Dori-4')),
        fill = '#F4BD0F'
    ) +
    facet_grid(. ~ name, scales = 'free_x')
```


![png](images/output_21_0.png)



```python
#@title {display-mode: "form"}

horeka <- df %>%
    filter(Channel == 'Horeka') %>%
    mutate(
        group_name = case_when(
            str_detect(`Customer Name`, '(APS|Ayam Penyet Surabaya)') ~ "aps",
            str_detect(`Customer Name`, 'Ampera') ~ "ampera",
            str_detect(`Customer Name`, 'KQ5') ~ "kq5",
            str_detect(`Customer Name`, '(WS|Wong Solo)') ~ "wong solo",
            str_detect(`Customer Name`, 'Penyetan Cok') ~ "penyetan cok",
            str_detect(`Customer Name`, 'Sambal Lalap') ~ "sambal lalap",
            str_detect(`Customer Name`, 'D Cost') ~ "dcost",
            str_detect(`Customer Name`, 'Ngikan') ~ "ngikan",
            TRUE ~ "other"
        )
    )
```
Pemesanan dori dengan ukuran 4 ternyata didominasi oleh Ngikan dan DSN (Monty) berdasarkan grafik di bawah ini. Grafik di bawah mewakili ~93% pasar ikan dori ukuran 4. Ngikan Buah Batu dan DSN (Monty) sendiri mencakup ~73% dari pasar komoditas ini.


```python
#@title {display-mode: "form"}

horeka %>%
    filter(
        Commodity == 'Dori',
        `Actual Size` == 4
    ) %>%
    group_by(`Customer Name`) %>%
    summarize(
        request_total = sum(request_weight_ton),
        .groups = 'drop_last'
    ) %>%
    mutate(
        percentage = request_total / sum(request_total)
    ) %>%
    arrange(-request_total) %>%
    filter(
        request_total > 0.1
    ) %>%
    ggplot(aes(x = request_total, y = reorder(`Customer Name`, request_total))) +
    geom_col() +
    geom_text(aes(label = scales::percent(percentage)), hjust = -.1, size = 5) +
    labs(
        x = 'request weight (ton)', y = '',
        title = "Ngikan Buah Batu dan DSN (Monty) menguasai mayoritas (~73%) komoditas ikan dori ukuran 4",
        subtitle = "Sekitar 93% pasar komoditas ini dikuasai oleh 7 restoran saja"
    )
```


![png](images/output_24_0.png)


Kalau dilihat berdasarkan grupnya, D'Cost menjadi penyumbang revenue terbesar sejauh ini meski frekuensi pemesanannya sangat kecil. Ayam Penyet Surabaya dan KQ5 mengikuti di belakangnya. Namun, potensi besar lain datang dari Ngikan yang secara frekuensi masih sedikit, tapi total tonnase dan revenue yang dihasilkan mengikuti pola D'Cost.


```python
#@title {display-mode: "form"}

horeka %>%
    group_by(group_name) %>%
    summarize(
        frequency = n(),
        `total weight (ton)` = sum(request_weight_ton, na.rm = TRUE),
        `total revenue` = sum(`Actual Total Price`, na.rm = TRUE),
        .groups = 'keep'
    ) %>%
    pivot_longer(-(group_name)) %>%
    ggplot(mapping = aes(x = value, y = group_name)) +
    geom_col() +
    geom_col(
        data = . %>% filter(group_name == 'dcost'),
        fill = '#28B796'
    ) +
    geom_col(
        data = . %>% filter(group_name == 'ngikan'),
        fill = '#F4BD0F'
    ) +
    facet_grid(. ~ name, scales = 'free_x') +
    labs(y = '')
```


![png](images/output_26_0.png)



```python
#@title {display-mode: "form"}

horeka %>%
    filter(group_name == 'dcost') %>%
    group_by(Commodity, `Actual Size`) %>%
    summarize(
        request_total = sum(request_weight_ton),
        .groups = 'drop_last'
    ) %>%
    unite(sku, c(Commodity, `Actual Size`), sep = '-') %>%
    ggplot(aes(x = request_total, y = sku)) +
    geom_col() +
    labs(
        x = 'request total (ton)', y = '',
        title = "D'Cost sebagai grup dengan sumber revenue terbesar paling banyak memesan ikan nila"
    )
```


![png](images/output_27_0.png)


Dengan mengecualikan permintaan dari D'Cost dan Ngikan yang frekuensinya kecil tapi secara tonnase besar, untuk permintaan dari horeka lainnya gurame menempati peringkat pertama dari tonnase. Kuantitasnya mencapai 2 kali lipat dari lele yang ada di peringkat kedua.


```python
#@title {display-mode: "form"}

horeka %>%
    filter(!is.na(Commodity)) %>%
    group_by(Commodity) %>%
    filter(!(group_name %in% c('ngikan', 'dcost'))) %>%
    summarize(
        total_request = sum(request_weight_ton, na.rm = TRUE),
        .groups = 'keep'
    ) %>%
    arrange(-total_request) %>%
    head() %>%
    ggplot() +
    geom_col(aes(x = total_request, y = reorder(Commodity, total_request))) +
    labs(
        x = 'total weight (ton)', y = '',
        title = "Permintaan gurame mencapai 2x lele jika mengecualikan Ngikan dan D'Cost"
    )
```


![png](images/output_29_0.png)



```python
#@title {display-mode: "form"}

library(survival)

surv_df <- horeka %>%
    group_by(Commodity) %>%
    filter(max(lubridate::year(`Transact Out Date`)) == 2020) %>%
    group_by(Commodity, `Transact Out Date`) %>%
    summarize(
        request_weight_ton = sum(request_weight_ton),
        .groups = 'drop_last'
    ) %>%
    mutate(
        diff = lead(`Transact Out Date`) - `Transact Out Date`,
        observed = !is.na(diff),
        diff = as.numeric(
            if_else(observed, diff, lubridate::today() - `Transact Out Date`)
        )
    )

km_fit <- survfit(Surv(diff, observed) ~ Commodity, data = surv_df)
```

```python
#@title Waktu yang dibutuhkan sampai pemesanan berikutnya per komoditas {display-mode: "form"}

library(broom)

tidy(km_fit) %>%
    ggplot(aes(x = time, y = 1 - estimate)) +
    geom_line(aes(color = strata)) +
    geom_ribbon(aes(x = time, ymin = 1 - conf.low, ymax = 1 - conf.high, fill = strata), alpha = 0.2) +
    xlim(0, 50) +
    labs(x = 'hari', y = 'p(transaksi)')
```


![png](images/output_31_1.png)


## Akhirnya

### Kesimpulan

1. Gurame merupakan komoditas yang paling banyak dicari oleh agen dan horeka. Permintaan untuk komoditas ini berulang, jadi besar kemungkinannya untuk ada dalam waktu yang cukup lama ke depannya.
2. Dori menjadi favorit bagi Ngikan sebagai [pemain baru](https://food.detik.com/resto-dan-kafe/d-4782701/ngikan-rela-antre-25-jam-demi-fish--chips-ala-indonesia-racikan-rachel-vennya) di horeka. Pemesanannya dilakukan dalam kuantatis besar meski secara jumlah pemesanan belum terlihat berulang seperti komoditas lainnya.
3. Bagi D'Cost sebagai grup dengan revenue terbesar saat ini, ikan nila justru menjadi komoditas utama yang dicari selain dori.
4. Processing sebagai channel yang melakukan pemesanan dengan frekuensi yang relatif cukup banyak jika dibandingkan dengan sisanya (kecuali horeka) membutuhkan patin dalam kuantitas yang cukup besar - 2x dari horeka.
5. Lele tergolong komoditas yang sering dipesan dengan tonnase relatif besar, tetapi secara revenue bukan yang paling besar.
6. Masih banyak pemesanan dalam jumlah besar yang dilakukan dengan ukuran 0. Ini menyulitkan untuk mengetahui sebetulnya permintaannya seperti apa.

### Saran dan Rekomendasi

1. Untuk memudahkan pemesanan ke petani, sebaiknya pencatatan ukuran dilengkapi lagi. Hindari penggunaan ukuran 0.
2. Dalam jangka pendek, memaksimalkan produksi gurame dan dori/patin dapat memaksimalkan revenue.
3. Analisis ini dapat dilengkapi dengan menambahkan ongkos produksi agar dapat melihat margin keuntungan yang dihasilkan, bukan hanya revenue.