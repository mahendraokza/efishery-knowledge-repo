---
title: Analisis Utilisasi Transaksi Feed & Fund
authors:
- Nurlaili
- Ali Akbar Septiandri
tags:
- knowledge
- utilisasi
- feedfund
created_at: 2020-05-28 00:00:00
updated_at: 2020-07-07 17:32:47.331645
tldr: This is short description of the content and findings of the post.
thumbnail: images/output_10_0.png
---

## Permasalahan


### Latar Belakang Masalah
Program eFishery Kabayan udah berjalan dari awal tahun 2020. Pendanaan untuk petani beli pakan udah banyak yang turun dari partner (berupa approved limit), tapi utilisasi nya masih rendah. Transaksi pakan belum banyak dilakukan petani, uang yang "udah ada" ini tidak termanfaatkan dengan maksimal

### Dugaan Masalah
Ada hal-hal tertentu yang bisa kita identifikasi untuk menemukan penyebabnya. Kemungkinannya bisa dilihat dari data karakteristik petani atau dari pola transaksinya itu sendiri

### Batasan Masalah
Sumber data petani diambil dari daftar nominatif. Untuk petani selain Alami kebanyakan datanya gak lengkap.

### Kamus Istilah
- daftar nominatif: data pengajuan pembiayaan berupa data-data pribadi petani, data budidaya, kebutuhan biaya, dll 

## Pengumpulan Data

### Sumber Datanya
Data diambil dari agregat data order petani beserta limitnya dan digabungkan dengan data pengajuan petani 

### Pengertian Sumber Data
- first_disbursement_date
- first_transaction_date
- 

## Eksplorasi Data


```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

%matplotlib inline
%config InlineBackend.figure_format = 'retina'
```

```python
sns.set_context('talk')
plt.style.use('ggplot')
plt.rcParams['figure.figsize'] = (12,7)
```

```python
#data loaded on May 28, ~10:00 AM
#re-loaded on June 03 
df = pd.read_csv('https://bi-dash.efishery.com/public/question/98f975fd-de02-4b48-b841-7efc894bdfcd.csv')
df = df.query('luas_lahan_m2 < 1.2e5')
df = df.query('total_kebutuhan_pakan < 1e9')
df['no_ktp'] = df['no_ktp'].astype('category')
df['is_transacted'] = df['jumlah_transaksi'] > 0
df['Limit transaksi'] /= 1e6
df['Total transaksi'] /= 1e6
df['Current saldo'] /= 1e6
df['Unpaid Amount'] /= 1e6
df['kebutuhan_pakan_ton'] = df['kebutuhan_pakan_kg'] / 1e3
df['total_kebutuhan_pakan'] /= 1e6
df['ltv_80'] /= 1e6
df['perkiraan_hasil_panen_ton'] = df['perkiraan_hasil_panen_kg'] / 1e3
df['total_hasil_panen'] /= 1e6
df['first_disbursement_date'] = pd.to_datetime(df['first_disbursement_date'], utc='UTC')
df['first_transaction_date'] = pd.to_datetime(df['first_transaction_date'])
df['usia'] = (pd.Timestamp.today() - pd.to_datetime(df['tanggal_lahir'])).dt.days / 365
```

```python
df.head(10)
```

```python
df.columns
```
## Transaksi


```python
from scipy.stats import expon

#dist jumlah transaksi per petani
sns.distplot(df['jumlah_transaksi'], fit=expon);
```


![png](images/output_10_0.png)


Petani juga masih rendah transaksinya, kebanyakan baru sekali doang



```python
df['is_transacted'].value_counts(True)
```




    True     0.546012
    False    0.453988
    Name: is_transacted, dtype: float64




```python
#jumlah petani yang transaksi & belum per Point nya
ax = sns.countplot(y='hub_id',hue='is_transacted',data=df)
plt.legend(fontsize=15, title='is_transacted',title_fontsize=15,loc=1)
plt.tight_layout()
plt.show()
```


![png](images/output_13_0.png)


Grafik diatas menunjukkan bahwa baru 5 dari 13 point yang petaninya sudah bertransaksi; Pamijahan menjadi point yang paling banyak petani aktif nya 


```python
sns.barplot(
    x='Total transaksi',
    y='hub_id',
    data=df
);
```


![png](images/output_15_0.png)


Meski Pamijahan paling banyak jumlah transaksinya, tapi Tulungagung ternyata yang paling besar rata-rata total nilai transaksinya.


```python
sns.barplot(
    x='Limit transaksi',
    y='hub_id',
    data=df
);
```


![png](images/output_17_0.png)


Secara limit, Pamijahan memang lebih sedikit. Tulungagung lebih besar secara limit, jadi cukup wajar kalau total nilai transaksinya juga lebih besar.

## Limit transaksi


```python
df.query('jumlah_transaksi > 0').groupby('hub_id').agg({
    'Limit transaksi': 'mean'
}).style.format('{:,.2f}')
```




<style  type="text/css" >
</style><table id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804" ><thead>    <tr>        <th class="blank level0" ></th>        <th class="col_heading level0 col0" >Limit transaksi</th>    </tr>    <tr>        <th class="index_name level0" >hub_id</th>        <th class="blank" ></th>    </tr></thead><tbody>
                <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row0" class="row_heading level0 row0" >Indramayu</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row0_col0" class="data row0 col0" >107.48</td>
            </tr>
            <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row1" class="row_heading level0 row1" >Palas</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row1_col0" class="data row1 col0" >105.30</td>
            </tr>
            <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row2" class="row_heading level0 row2" >Pamijahan</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row2_col0" class="data row2 col0" >25.78</td>
            </tr>
            <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row3" class="row_heading level0 row3" >Pringsewu</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row3_col0" class="data row3 col0" >74.30</td>
            </tr>
            <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row4" class="row_heading level0 row4" >Subang</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row4_col0" class="data row4 col0" >68.37</td>
            </tr>
            <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row5" class="row_heading level0 row5" >Tasikmalaya</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row5_col0" class="data row5 col0" >12.12</td>
            </tr>
            <tr>
                        <th id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804level0_row6" class="row_heading level0 row6" >Tulungagung</th>
                        <td id="T_478cd2a0_aa2c_11ea_8f39_70bc1070a804row6_col0" class="data row6 col0" >134.64</td>
            </tr>
    </tbody></table>




```python
sns.scatterplot(
    'Limit transaksi',
    'Total transaksi',
    hue='hub_id',
    data=df[df['jumlah_transaksi'] > 0]
);
```


![png](images/output_21_0.png)



```python
# sns.lmplot(
#     'Limit transaksi',
#     'Total transaksi',
#     data=df[df['jumlah_transaksi'] > 0],
#     hue='hub_id'
# );
```
Limit vs Total transaksi = secara umum limit memengaruhi transaksi, semakin besar limit akan men-trigger petani untuk bertransaksi lebih besar. Tapi ada pengecualian untuk Point Palas yang limitnya besar tapi total transaksi masih sedikit

## Distribusi data


```python
columns = [
    'hub_id',
    'jumlah_transaksi',
    'Limit transaksi',
    'luas_lahan_m2',
    'jumlah_kolam_aktif',
    'total_kebutuhan_pakan_ton',
    'harga_pakan_kg',
    'perkiraan_waktu_panen_hari'
]
```

```python
#ini fungsi draw distplot per hub
def draw_per_hub_id(col):
    for hub_id, group in df.groupby('hub_id'):
        if group.shape[0] == 1:
            continue
        sns.distplot(group[col], hist=False, label=hub_id)
    plt.legend();
```

```python
from scipy.stats import norm

sns.distplot(
    df['usia'],
    bins=20,
    fit=norm
);
```


![png](images/output_27_0.png)



```python
df['usia'].agg(['mean','std'])
```




    mean    43.453016
    std     10.031038
    Name: usia, dtype: float64



Usia dari para pembudidaya rata-rata $43 \pm 10$ tahun.


```python
sns.boxplot(df['jumlah_transaksi']);
```


![png](images/output_30_0.png)



```python
sns.boxplot(df['Limit transaksi']);
```


![png](images/output_31_0.png)


## Based on Komoditas


```python
fish_names = [
    'mas',
    'nila',
    'patin',
    'lele',
    'gabus',
    'bandeng',
    'gurame',
    'mujaer'
]

for fish in fish_names:
    df[fish] = df['komoditas'].str.lower().str.contains(fish)
```

```python
X = pd.concat([
    df[['hub_id']],
    df[fish_names]
], axis=1)
```

```python
sns.heatmap(
    X.groupby('hub_id').agg(dict(
        zip(fish_names, ['sum' for _ in range(len(fish_names))])
    )).astype('float'),
    annot=True,
    fmt='.0f',
    cmap='Blues'
)
plt.yticks(rotation=0);
```


![png](images/output_35_0.png)



```python
sns.heatmap(
    df[fish_names + ['Total transaksi']].corr(),
    center=0,
    annot=True,
    fmt='.2f',
    square=True
);
```


![png](images/output_36_0.png)


Tulungagung yang mayoritas komoditasnya adalah patin melakukan transaksi dengan nilai yang cukup besar, tapi tidak dengan Palas yang komoditasnya sama. Mengapa?


```python
import statsmodels.formula.api as smf
import statsmodels.api as sm

mdl = smf.ols(
    'jumlah_transaksi ~ {}'.format(' + '.join(fish_names)),
    df
).fit()
mdl.summary()
```




<table class="simpletable">
<caption>OLS Regression Results</caption>
<tr>
  <th>Dep. Variable:</th>    <td>jumlah_transaksi</td> <th>  R-squared:         </th> <td>   0.255</td>
</tr>
<tr>
  <th>Model:</th>                   <td>OLS</td>       <th>  Adj. R-squared:    </th> <td>   0.207</td>
</tr>
<tr>
  <th>Method:</th>             <td>Least Squares</td>  <th>  F-statistic:       </th> <td>   5.345</td>
</tr>
<tr>
  <th>Date:</th>             <td>Tue, 09 Jun 2020</td> <th>  Prob (F-statistic):</th> <td>8.80e-06</td>
</tr>
<tr>
  <th>Time:</th>                 <td>15:39:04</td>     <th>  Log-Likelihood:    </th> <td> -352.26</td>
</tr>
<tr>
  <th>No. Observations:</th>      <td>   134</td>      <th>  AIC:               </th> <td>   722.5</td>
</tr>
<tr>
  <th>Df Residuals:</th>          <td>   125</td>      <th>  BIC:               </th> <td>   748.6</td>
</tr>
<tr>
  <th>Df Model:</th>              <td>     8</td>      <th>                     </th>     <td> </td>   
</tr>
<tr>
  <th>Covariance Type:</th>      <td>nonrobust</td>    <th>                     </th>     <td> </td>   
</tr>
</table>
<table class="simpletable">
<tr>
         <td></td>            <th>coef</th>     <th>std err</th>      <th>t</th>      <th>P>|t|</th>  <th>[0.025</th>    <th>0.975]</th>  
</tr>
<tr>
  <th>Intercept</th>       <td>   -0.0626</td> <td>    1.013</td> <td>   -0.062</td> <td> 0.951</td> <td>   -2.067</td> <td>    1.942</td>
</tr>
<tr>
  <th>mas[T.True]</th>     <td>    3.4751</td> <td>    0.879</td> <td>    3.953</td> <td> 0.000</td> <td>    1.735</td> <td>    5.215</td>
</tr>
<tr>
  <th>nila[T.True]</th>    <td>    1.3176</td> <td>    0.890</td> <td>    1.480</td> <td> 0.141</td> <td>   -0.444</td> <td>    3.079</td>
</tr>
<tr>
  <th>patin[T.True]</th>   <td>    0.5934</td> <td>    1.030</td> <td>    0.576</td> <td> 0.566</td> <td>   -1.445</td> <td>    2.632</td>
</tr>
<tr>
  <th>lele[T.True]</th>    <td>    0.0368</td> <td>    0.920</td> <td>    0.040</td> <td> 0.968</td> <td>   -1.785</td> <td>    1.858</td>
</tr>
<tr>
  <th>gabus[T.True]</th>   <td>   -0.5308</td> <td>    3.520</td> <td>   -0.151</td> <td> 0.880</td> <td>   -7.498</td> <td>    6.436</td>
</tr>
<tr>
  <th>bandeng[T.True]</th> <td>   -0.5962</td> <td>    2.579</td> <td>   -0.231</td> <td> 0.818</td> <td>   -5.700</td> <td>    4.508</td>
</tr>
<tr>
  <th>gurame[T.True]</th>  <td>   -0.2241</td> <td>    1.805</td> <td>   -0.124</td> <td> 0.901</td> <td>   -3.796</td> <td>    3.348</td>
</tr>
<tr>
  <th>mujaer[T.True]</th>  <td>   -1.2549</td> <td>    3.560</td> <td>   -0.353</td> <td> 0.725</td> <td>   -8.300</td> <td>    5.790</td>
</tr>
</table>
<table class="simpletable">
<tr>
  <th>Omnibus:</th>       <td>86.766</td> <th>  Durbin-Watson:     </th> <td>   0.313</td>
</tr>
<tr>
  <th>Prob(Omnibus):</th> <td> 0.000</td> <th>  Jarque-Bera (JB):  </th> <td> 448.510</td>
</tr>
<tr>
  <th>Skew:</th>          <td> 2.370</td> <th>  Prob(JB):          </th> <td>4.05e-98</td>
</tr>
<tr>
  <th>Kurtosis:</th>      <td>10.607</td> <th>  Cond. No.          </th> <td>    14.9</td>
</tr>
</table><br/><br/>Warnings:<br/>[1] Standard Errors assume that the covariance matrix of the errors is correctly specified.




```python
columns = [
    'hub_id',
    'jumlah_transaksi',
    'Limit transaksi',
    'luas_lahan_m2',
    'jumlah_kolam_aktif',
    'total_kebutuhan_pakan_ton',
    'harga_pakan_kg',
    'perkiraan_waktu_panen_hari'
]
```

```python
sns.heatmap(
    df[df.columns[:-4]].corr(),
    square=True,
    center=0
);
```


![png](images/output_40_0.png)



```python
sns.regplot(
    'total_kebutuhan_pakan',
    'Limit transaksi',
    data=df
);
#abaikan total kebutuhan pakan berarti. udah di represent sama limit
```


![png](images/output_41_0.png)



```python
import statsmodels.formula.api as smf
import statsmodels.api as sm

mdl = smf.ols(
    'total_kebutuhan_pakan ~ jumlah_kolam_aktif',
    df.query('jumlah_kolam_aktif < 50')
).fit()
mdl.summary()
```




<table class="simpletable">
<caption>OLS Regression Results</caption>
<tr>
  <th>Dep. Variable:</th>    <td>total_kebutuhan_pakan</td> <th>  R-squared:         </th> <td>   0.352</td>
</tr>
<tr>
  <th>Model:</th>                     <td>OLS</td>          <th>  Adj. R-squared:    </th> <td>   0.346</td>
</tr>
<tr>
  <th>Method:</th>               <td>Least Squares</td>     <th>  F-statistic:       </th> <td>   65.60</td>
</tr>
<tr>
  <th>Date:</th>               <td>Tue, 09 Jun 2020</td>    <th>  Prob (F-statistic):</th> <td>5.00e-13</td>
</tr>
<tr>
  <th>Time:</th>                   <td>15:39:08</td>        <th>  Log-Likelihood:    </th> <td> -785.02</td>
</tr>
<tr>
  <th>No. Observations:</th>        <td>   123</td>         <th>  AIC:               </th> <td>   1574.</td>
</tr>
<tr>
  <th>Df Residuals:</th>            <td>   121</td>         <th>  BIC:               </th> <td>   1580.</td>
</tr>
<tr>
  <th>Df Model:</th>                <td>     1</td>         <th>                     </th>     <td> </td>   
</tr>
<tr>
  <th>Covariance Type:</th>        <td>nonrobust</td>       <th>                     </th>     <td> </td>   
</tr>
</table>
<table class="simpletable">
<tr>
           <td></td>             <th>coef</th>     <th>std err</th>      <th>t</th>      <th>P>|t|</th>  <th>[0.025</th>    <th>0.975]</th>  
</tr>
<tr>
  <th>Intercept</th>          <td>   69.6628</td> <td>   18.835</td> <td>    3.699</td> <td> 0.000</td> <td>   32.374</td> <td>  106.951</td>
</tr>
<tr>
  <th>jumlah_kolam_aktif</th> <td>   15.5101</td> <td>    1.915</td> <td>    8.099</td> <td> 0.000</td> <td>   11.719</td> <td>   19.301</td>
</tr>
</table>
<table class="simpletable">
<tr>
  <th>Omnibus:</th>       <td>54.252</td> <th>  Durbin-Watson:     </th> <td>   1.847</td>
</tr>
<tr>
  <th>Prob(Omnibus):</th> <td> 0.000</td> <th>  Jarque-Bera (JB):  </th> <td> 140.000</td>
</tr>
<tr>
  <th>Skew:</th>          <td> 1.757</td> <th>  Prob(JB):          </th> <td>3.98e-31</td>
</tr>
<tr>
  <th>Kurtosis:</th>      <td> 6.869</td> <th>  Cond. No.          </th> <td>    14.3</td>
</tr>
</table><br/><br/>Warnings:<br/>[1] Standard Errors assume that the covariance matrix of the errors is correctly specified.




```python
sns.regplot(
    'total_kebutuhan_pakan',
    'jumlah_transaksi',
    data=df.query('jumlah_kolam_aktif < 50')
);
```


![png](images/output_43_0.png)


## Melihat berdasarkan waktu "Activation" dengan Survival Analysis 


```python
df['days_to_transact'] = (df['first_transaction_date'] - df['first_disbursement_date']).dt.days
df['observed'] = df['days_to_transact'].notnull()
df['duration'] = df['days_to_transact']
df.loc[~df['observed'],'duration'] = (pd.Timestamp.today(tz='utc') - df.loc[~df['observed'], 'first_disbursement_date']).dt.days
```

```python
from scipy.stats import expon

# Cara salah

sns.distplot(
    df['days_to_transact'],
    fit=expon
);
```


![png](images/output_46_0.png)



```python
!pip install lifelines
```

```python
from lifelines import KaplanMeierFitter

kmf = KaplanMeierFitter()
kmf.fit(df['duration'], df['observed'])
ax = plt.gca()
(1-kmf.survival_function_).plot.line(ax=ax)
x = kmf.survival_function_.index
plt.fill_between(x, *(1-kmf.confidence_interval_.values.T), alpha=.25)
plt.axvline(kmf.median_survival_time_, linestyle=':')
plt.xlabel('elapsed days from disbursement')
plt.ylabel('p(transact)')
print('Median: {:.0f} days'.format(kmf.median_survival_time_))
```
    Median: 31 days




![png](images/output_48_1.png)


Berdasarkan [survival analysis](https://en.wikipedia.org/wiki/Survival_analysis), dapat dilihat bahwa jumlah petani yang bertransaksi melalui Feed & Fund belum sampai setengahnya (garis merah tidak pernah menyentuh 0.5). Selain itu, kita dapat melihat dari grafik bahwa petani yang akhirnya melakukan transaksi cenderung melakukannya dalam 3-5 hari pertama setelah disbursement dilakukan.

Qualitatively speaking:
- simply karena Pamijahan udah launch duluan
- timing budidaya
- proses internal yang belum final (onboarding, masih kendala di harga, migrasi dr skema lama, dll)
- covid, ngaruh ke siklus budidaya, petani gak mau nebar, khawatir gaada serapan ikan
- kondisi petani: tidak ada gudang pakan, gabisa nyetok, mesen dikit2
- "Petani yang punya kebutuhan besar untuk pakan karena budidaya ikan size 1 Kg Up malah dapat baseline;"
- intervensi (?) regmem


https://efishery.slack.com/archives/CKX3E4MUJ/p1589531599179600

https://efishery.slack.com/archives/CKX3E4MUJ/p1590637269456800

https://docs.google.com/document/d/11hGV9Q5S3IreIMi27zbD8bZS8njuL05FZZw52DJ4V50/edit#heading=h.ev4lppc8mqf3

 ---

## Based on Data Order

Lihat pamijahan as success story?

Bandingin dengan Point lain?

Perbedaan karakteristik transaksi untuk masing2 Point?


```python
#@title Give me a name {display-mode: "form"}

# This code will be hidden when the notebook is loaded.

```

```python
dft = pd.read_csv('https://bi-dash.efishery.com/public/question/f69e8d6d-ba99-4374-8079-ac0cc6c6457a.csv')
dft['order_date'] = pd.to_datetime(dft['order_date']).dt.tz_localize(None)
```

```python
# !pip install lifetimes
```

```python
dft.set_index('order_date').resample('1d')['subtotal_petani'].sum().plot.line();
```


![png](images/output_58_0.png)



```python
from lifetimes.utils import calibration_and_holdout_data, summary_data_from_transaction_data

today = pd.Timestamp.today().strftime('%Y-%m-%d')

summary_cal_holdout = calibration_and_holdout_data(
    dft, 'customer_id', 'order_date',
    calibration_period_end='2020-04-30',
    observation_period_end=today,
    freq='W',
    monetary_value_col='margin'
)
```

```python
from lifetimes import BetaGeoFitter
from lifetimes.plotting import plot_calibration_purchases_vs_holdout_purchases

bgf = BetaGeoFitter(penalizer_coef=1)
bgf.fit(
    summary_cal_holdout['frequency_cal'],
    summary_cal_holdout['recency_cal'],
    summary_cal_holdout['T_cal']
)
plot_calibration_purchases_vs_holdout_purchases(bgf, summary_cal_holdout);
```


![png](images/output_60_0.png)



```python
summary = summary_data_from_transaction_data(
    dft, 'customer_id', 'order_date',
    observation_period_end=today,
    freq='W',
    monetary_value_col='subtotal_petani'
)
```

```python
bgf = BetaGeoFitter(penalizer_coef=1)
bgf.fit(
    summary['frequency'],
    summary['recency'],
    summary['T']
)

# sns.distplot(
#     bgf.conditional_probability_alive(
#         summary['frequency'],
#         summary['recency'],
#         summary['T']
#     )
# )

bgf.conditional_probability_alive(
    summary['frequency'],
    summary['recency'],
    summary['T']
)
```




    array([1.        , 1.        , 0.99999846, 1.        , 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 0.99999966, 0.99999969, 0.99999962, 1.        ,
           1.        , 1.        , 1.        , 1.        , 0.9999998 ,
           1.        , 1.        , 0.99999967, 1.        , 1.        ,
           1.        , 1.        , 1.        , 0.99999978, 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           0.99999953, 1.        , 1.        , 0.99999978, 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 1.        , 1.        , 1.        , 1.        ,
           1.        , 1.        ])




```python
summary[['monetary_value', 'frequency']].corr()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>monetary_value</th>
      <th>frequency</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>monetary_value</th>
      <td>1.000000</td>
      <td>0.204258</td>
    </tr>
    <tr>
      <th>frequency</th>
      <td>0.204258</td>
      <td>1.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
from lifetimes import GammaGammaFitter

ggf = GammaGammaFitter(penalizer_coef = 0)
ggf.fit(
    summary.query('monetary_value > 0')['frequency'],
    summary.query('monetary_value > 0')['monetary_value']
)
clv = ggf.customer_lifetime_value(
    bgf, #the model to use to predict the number of future transactions
    summary['frequency'],
    summary['recency'],
    summary['T'],
    summary['monetary_value'],
    time=12, # months
    discount_rate=0.01 # monthly discount rate ~ 12.7% annually
)
```

```python
p_alive = bgf.conditional_probability_alive(
    summary['frequency'],
    summary['recency'],
    summary['T']
)
```

```python
dft[dft['customer_id'] == 'CF19121132n4f0273'].set_index(
    'order_date'
).resample('1w').subtotal_petani.sum()
```




    order_date
    2020-02-09    1195950
    2020-02-16    6880100
    2020-02-23          0
    2020-03-01    7503050
    2020-03-08    5878500
    2020-03-15          0
    2020-03-22    2673650
    2020-03-29          0
    2020-04-05    4932000
    2020-04-12          0
    2020-04-19    4134700
    2020-04-26    8170600
    2020-05-03          0
    2020-05-10    3986500
    2020-05-17          0
    2020-05-24    4385150
    Freq: W-SUN, Name: subtotal_petani, dtype: int64




```python
fig, ax = plt.subplots(figsize=(12,3))
sns.boxplot(clv.dropna());
```


![png](images/output_67_0.png)



```python
summary['clv'] = clv
summary['p_alive'] = p_alive
```

```python
summary['clv_segment'] = pd.cut(
    summary['clv'],
    summary['clv'].quantile([0, 1/3, 2/3, 3/3])
).cat.codes
```

```python
summary['p_alive_segment'] = pd.cut(
    summary['p_alive'],
    [0, 1/3, 2/3, 1]
).cat.codes
```

```python
summary.pivot_table(
    index='p_alive_segment',
    columns='clv_segment',
    values='frequency',
    aggfunc='count'
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>clv_segment</th>
      <th>-1</th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
    </tr>
    <tr>
      <th>p_alive_segment</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2</th>
      <td>43</td>
      <td>9</td>
      <td>10</td>
      <td>10</td>
    </tr>
  </tbody>
</table>
</div>



Buy til you die (BG/NBD)

Jika dilihat dari yang sudah pernah transaksi saja, maka masih sangat besar kemungkinan bahwa mereka akan melakukan transaksi lagi ke depannya.


```python
#ini fungsi draw distplot per hub
def draw_per_hub_id(col,dat):
    for hub_id, group in dat.groupby('hub_id'):
        if group.shape[0] == 1:
            continue
        sns.distplot(group[col], hist=False, label=hub_id)
    plt.legend();
```

```python
draw_per_hub_id('margin', dft)
```


![png](images/output_75_1.png)



```python
dft.dtypes
```




    order_date           datetime64[ns, UTC]
    order_id                          object
    customer_id                       object
    jenis_barang                      object
    kode_barang                       object
    qty                                int64
    tenor                              int64
    unit_price_asli                    int64
    unit_price_petani                  int64
    margin                             int64
    subtotal_asli                      int64
    subtotal_petani                    int64
    hub_id                            object
    dtype: object




```python
sns.distplot(
    dft.sort_values(
        ['customer_id','order_date']
    ).groupby('customer_id').head(1).unit_price_asli
);
```


![png](images/output_77_0.png)



```python
sns.distplot(
    dft['kode_barang']
)
```


![png](images/output_78_0.png)



```python
sns.countplot(x=dft['order_date'].dt.month,hue='hub_id',data=dft)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fe0e7ce36d0>





![png](images/output_79_1.png)



```python
dft[dft['order_date'].dt.month==5].describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qty</th>
      <th>tenor</th>
      <th>unit_price_asli</th>
      <th>unit_price_petani</th>
      <th>margin</th>
      <th>subtotal_asli</th>
      <th>subtotal_petani</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>50.000000</td>
      <td>50.0000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.00000</td>
      <td>5.000000e+01</td>
      <td>5.000000e+01</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>56.040000</td>
      <td>3.6200</td>
      <td>322298.080000</td>
      <td>341796.120000</td>
      <td>19498.04000</td>
      <td>1.439416e+07</td>
      <td>1.537366e+07</td>
    </tr>
    <tr>
      <th>std</th>
      <td>109.251808</td>
      <td>1.2599</td>
      <td>67519.324563</td>
      <td>66012.987182</td>
      <td>8811.23114</td>
      <td>2.505330e+07</td>
      <td>2.654915e+07</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>1.0000</td>
      <td>214280.000000</td>
      <td>240500.000000</td>
      <td>8250.00000</td>
      <td>3.040000e+05</td>
      <td>3.155000e+05</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>5.250000</td>
      <td>3.0000</td>
      <td>275000.000000</td>
      <td>292821.750000</td>
      <td>12207.50000</td>
      <td>2.055000e+06</td>
      <td>2.116750e+06</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>15.000000</td>
      <td>3.0000</td>
      <td>300000.000000</td>
      <td>324445.000000</td>
      <td>16670.00000</td>
      <td>5.495850e+06</td>
      <td>5.795148e+06</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>40.000000</td>
      <td>4.0000</td>
      <td>405000.000000</td>
      <td>414100.000000</td>
      <td>27891.75000</td>
      <td>1.319400e+07</td>
      <td>1.406097e+07</td>
    </tr>
    <tr>
      <th>max</th>
      <td>500.000000</td>
      <td>6.0000</td>
      <td>414250.000000</td>
      <td>443528.000000</td>
      <td>36089.00000</td>
      <td>1.171000e+08</td>
      <td>1.250000e+08</td>
    </tr>
  </tbody>
</table>
</div>




```python
dft[dft['order_date'].dt.month==5].groupby('hub_id')['customer_id'].nunique()
```




    hub_id
    Indramayu       1
    Palas           4
    Pamijahan      11
    Subang          5
    Tulungagung     6
    Name: customer_id, dtype: int64




```python
#dft[dft['order_date'].dt.month==5]
dft.groupby('hub_id').agg({'subtotal_petani':'sum'}).sort_values(by='subtotal_petani').style.format('{:,.2f}')
```




<style  type="text/css" >
</style><table id="T_aeb3a926_a573_11ea_ba77_0242ac110003" ><thead>    <tr>        <th class="blank level0" ></th>        <th class="col_heading level0 col0" >subtotal_petani</th>    </tr>    <tr>        <th class="index_name level0" >hub_id</th>        <th class="blank" ></th>    </tr></thead><tbody>
                <tr>
                        <th id="T_aeb3a926_a573_11ea_ba77_0242ac110003level0_row0" class="row_heading level0 row0" >Indramayu</th>
                        <td id="T_aeb3a926_a573_11ea_ba77_0242ac110003row0_col0" class="data row0 col0" >37,939,200.00</td>
            </tr>
            <tr>
                        <th id="T_aeb3a926_a573_11ea_ba77_0242ac110003level0_row1" class="row_heading level0 row1" >Palas</th>
                        <td id="T_aeb3a926_a573_11ea_ba77_0242ac110003row1_col0" class="data row1 col0" >103,067,441.00</td>
            </tr>
            <tr>
                        <th id="T_aeb3a926_a573_11ea_ba77_0242ac110003level0_row2" class="row_heading level0 row2" >Subang</th>
                        <td id="T_aeb3a926_a573_11ea_ba77_0242ac110003row2_col0" class="data row2 col0" >127,430,340.00</td>
            </tr>
            <tr>
                        <th id="T_aeb3a926_a573_11ea_ba77_0242ac110003level0_row3" class="row_heading level0 row3" >Pamijahan</th>
                        <td id="T_aeb3a926_a573_11ea_ba77_0242ac110003row3_col0" class="data row3 col0" >560,211,970.00</td>
            </tr>
            <tr>
                        <th id="T_aeb3a926_a573_11ea_ba77_0242ac110003level0_row4" class="row_heading level0 row4" >Tulungagung</th>
                        <td id="T_aeb3a926_a573_11ea_ba77_0242ac110003row4_col0" class="data row4 col0" >811,175,000.00</td>
            </tr>
    </tbody></table>




```python
dft.groupby('hub_id').agg({'subtotal_petani':'sum','margin':'mean','qty':'sum'}).sort_values(by='margin').style.format('{:,.2f}')
```




<style  type="text/css" >
</style><table id="T_b591b224_a573_11ea_ba77_0242ac110003" ><thead>    <tr>        <th class="blank level0" ></th>        <th class="col_heading level0 col0" >subtotal_petani</th>        <th class="col_heading level0 col1" >margin</th>        <th class="col_heading level0 col2" >qty</th>    </tr>    <tr>        <th class="index_name level0" >hub_id</th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>    </tr></thead><tbody>
                <tr>
                        <th id="T_b591b224_a573_11ea_ba77_0242ac110003level0_row0" class="row_heading level0 row0" >Pamijahan</th>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row0_col0" class="data row0 col0" >560,211,970.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row0_col1" class="data row0 col1" >12,156.75</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row0_col2" class="data row0 col2" >1,467.00</td>
            </tr>
            <tr>
                        <th id="T_b591b224_a573_11ea_ba77_0242ac110003level0_row1" class="row_heading level0 row1" >Tulungagung</th>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row1_col0" class="data row1 col0" >811,175,000.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row1_col1" class="data row1 col1" >13,411.67</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row1_col2" class="data row1 col2" >3,350.00</td>
            </tr>
            <tr>
                        <th id="T_b591b224_a573_11ea_ba77_0242ac110003level0_row2" class="row_heading level0 row2" >Indramayu</th>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row2_col0" class="data row2 col0" >37,939,200.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row2_col1" class="data row2 col1" >16,840.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row2_col2" class="data row2 col2" >130.00</td>
            </tr>
            <tr>
                        <th id="T_b591b224_a573_11ea_ba77_0242ac110003level0_row3" class="row_heading level0 row3" >Subang</th>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row3_col0" class="data row3 col0" >127,430,340.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row3_col1" class="data row3 col1" >22,281.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row3_col2" class="data row3 col2" >360.00</td>
            </tr>
            <tr>
                        <th id="T_b591b224_a573_11ea_ba77_0242ac110003level0_row4" class="row_heading level0 row4" >Palas</th>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row4_col0" class="data row4 col0" >103,067,441.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row4_col1" class="data row4 col1" >33,082.00</td>
                        <td id="T_b591b224_a573_11ea_ba77_0242ac110003row4_col2" class="data row4 col2" >334.00</td>
            </tr>
    </tbody></table>




```python
dft[dft['hub_id']=='Pamijahan'].describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qty</th>
      <th>tenor</th>
      <th>unit_price_asli</th>
      <th>unit_price_petani</th>
      <th>margin</th>
      <th>subtotal_asli</th>
      <th>subtotal_petani</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>194.000000</td>
      <td>194.000000</td>
      <td>194.000000</td>
      <td>194.000000</td>
      <td>194.000000</td>
      <td>1.940000e+02</td>
      <td>1.940000e+02</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>7.561856</td>
      <td>2.912371</td>
      <td>371994.845361</td>
      <td>384151.597938</td>
      <td>12156.752577</td>
      <td>2.787567e+06</td>
      <td>2.887691e+06</td>
    </tr>
    <tr>
      <th>std</th>
      <td>10.275923</td>
      <td>0.363563</td>
      <td>49477.424320</td>
      <td>49260.716080</td>
      <td>2125.193504</td>
      <td>3.353602e+06</td>
      <td>3.532226e+06</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>260000.000000</td>
      <td>272160.000000</td>
      <td>4000.000000</td>
      <td>3.040000e+05</td>
      <td>3.155000e+05</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>3.000000</td>
      <td>3.000000</td>
      <td>304000.000000</td>
      <td>316500.000000</td>
      <td>11650.000000</td>
      <td>9.120000e+05</td>
      <td>9.465000e+05</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>5.000000</td>
      <td>3.000000</td>
      <td>387000.000000</td>
      <td>398650.000000</td>
      <td>12350.000000</td>
      <td>1.935000e+06</td>
      <td>1.993250e+06</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>10.000000</td>
      <td>3.000000</td>
      <td>411000.000000</td>
      <td>423350.000000</td>
      <td>12350.000000</td>
      <td>3.645000e+06</td>
      <td>3.754575e+06</td>
    </tr>
    <tr>
      <th>max</th>
      <td>96.000000</td>
      <td>3.000000</td>
      <td>430000.000000</td>
      <td>440300.000000</td>
      <td>22800.000000</td>
      <td>2.976000e+07</td>
      <td>3.189880e+07</td>
    </tr>
  </tbody>
</table>
</div>



## Akhirnya

### Kesimpulan

### Saran dan Rekomendasi


## Referensi