---
title: Data Komparasi untuk Canvassing Sales Area
authors:
- Ali Akbar Septiandri
tags:
- knowledge
- canvassing
- sales
created_at: 2020-04-13 00:00:00
updated_at: 2020-07-07 17:27:28.820201
tldr: This is short description of the content and findings of the post.
thumbnail: images/output_24_0.png
---

## Permasalahan

### Latar Belakang Masalah
eFishery telah melakukan survey area baru dengan mendatangi langsung dan by call. Tujuannya untuk mendapatkan info dasar di area tersebut, seperti ada berapa petani, berapa kolam, produksi ikan, dan pejualan ikan. 

### Dugaan Masalah
Sekarang kita butuh untuk menunjukkan hasil eksplorasi data untuk bisa dibaca oleh eksekutif atau tim eFishery dari hasil pengumpulan datanya

### Batasan Masalah
Eksplorasi yang dilakukan ditujukan untuk komparasi data dengan data bi-dash Canvassing Sales Area.
### Kamus Istilah

### Pengertian Sumber Data
Data yang didapatkan dari http://canvassing.tools.efishery.com oleh tim lapangan dimasukkan dan disiapkan melalui bi-dash Metabase https://bi-dash.efishery.com/question/3001 dengan export public menjadi CSV di https://bi-dash.efishery.com/public/question/2ed251be-6f7d-4bb4-9c73-66f3c5bbf57c.csv

## Eksplorasi Data

### Mari kita lihat datanya


```python
# Import Library
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import ast, json
from datetime import datetime
%matplotlib inline
```

```python
sns.set_context('talk')
plt.rcParams['figure.figsize'] = (12,7)
```

```python
# load data
df_canvassing = pd.read_csv('https://docs.google.com/spreadsheets/d/e/2PACX-1vRJa2phEwO63FLMXBZdMxLBa9yM5A994i5eWr8GoPrPD1_sr14JO0JpulnfRBXdoabjHMpoV_iQmcXj/pub?gid=1532506206&single=true&output=csv')
print(df_canvassing.shape)
df_canvassing.head(2)
```
    (50, 12)






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Timestamp</th>
      <th>Nama kandidat area</th>
      <th>Province</th>
      <th>Fase berapa area ini saat peluncuran?</th>
      <th>Berapa total petani yang sudah menggunakan eFishery di area tersebut? (jika belum ada, tulis 0)</th>
      <th>Jabarkan alasan area ini diajukan dan fase tersebut di atas dipilih.</th>
      <th>Berapa estimasi total petani di area tersebut?</th>
      <th>Berapa estimasi total kolam di area tersebut?</th>
      <th>Berapa estimasi total produksi ikan di area tersebut?</th>
      <th>Berapa estimasi total penjualan pakan di area tersebut?</th>
      <th>Apa komoditas utama yang diproduksi di area tersebut?</th>
      <th>Siapa market leader pakan di area tersebut? (Pilih maksimal 3)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4/9/2020 11:54:46</td>
      <td>Narmada</td>
      <td>NTB</td>
      <td>F4</td>
      <td>4</td>
      <td>Sudah melakukan penjajakan dari awal bulan Jan...</td>
      <td>20</td>
      <td>40</td>
      <td>100</td>
      <td>150</td>
      <td>Nila</td>
      <td>CPP, STP, Global</td>
    </tr>
    <tr>
      <th>1</th>
      <td>4/9/2020 16:31:22</td>
      <td>Garut</td>
      <td>Jabar</td>
      <td>F1</td>
      <td>1</td>
      <td>Garut termasuk yg perikanannya cukup besar set...</td>
      <td>150</td>
      <td>800</td>
      <td>300</td>
      <td>100</td>
      <td>Nila, Mas, Nilem</td>
      <td>CPP, Sinta</td>
    </tr>
  </tbody>
</table>
</div>



hmm, nama kolomnya terlalu panjang. Untuk mempermudah, selanjutnya kita akan memodifikasi nama kolomnya ya.


```python
df_canvassing = pd.read_csv(
    'https://docs.google.com/spreadsheets/d/e/2PACX-1vRJa2phEwO63FLMXBZdMxLBa9yM5A994i5eWr8GoPrPD1_sr14JO0JpulnfRBXdoabjHMpoV_iQmcXj/pub?gid=1532506206&single=true&output=csv',
    skiprows=1,
    names=[
        'date',
        'kandidat_area',
        'provinsi',
        'fase_peluncuran',
        'jumlah_petani_efishery',
        'alasan_dianjurkan',
        'total_jumlah_petani',
        'jumlah_kolam',
        'produksi_ikan',
        'penjualan_ikan',
        'komoditas_utama',
        'market_leader_pakan'
    ]
)
```
Sekarang nama kolomnya sudah nyaman untuk digunakan. Selanjutnya, karena ini bukan termasuk data **time series** (tidak ada keterkaitan antar observasinya), jadi disini kita akan hilangkan kolom tanggalnya.

Dari raw data diatas, ada beberapa kolom yang harus diberikan pengolahan khusus, seperti fase_peluncuran, alasan_dianjurkan, komoditas utama, market_leader_pakan. Agar lebih mudah dianalisis.

Tapi untuk sekarang, kita lihat dulu pada data numeriknya, dan distribusi daerahnya.


```python
# cek keberadaan kolom
print(df_canvassing.info())
print("Total data:" + str(len(df_canvassing)))
print("Total data unique area:" + str(df_canvassing['provinsi'].nunique()))
```
    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 50 entries, 0 to 49
    Data columns (total 12 columns):
     #   Column                  Non-Null Count  Dtype 
    ---  ------                  --------------  ----- 
     0   date                    10 non-null     object
     1   kandidat_area           50 non-null     object
     2   provinsi                50 non-null     object
     3   fase_peluncuran         50 non-null     object
     4   jumlah_petani_efishery  50 non-null     int64 
     5   alasan_dianjurkan       50 non-null     object
     6   total_jumlah_petani     50 non-null     int64 
     7   jumlah_kolam            50 non-null     int64 
     8   produksi_ikan           50 non-null     int64 
     9   penjualan_ikan          50 non-null     int64 
     10  komoditas_utama         50 non-null     object
     11  market_leader_pakan     50 non-null     object
    dtypes: int64(5), object(7)
    memory usage: 4.8+ KB
    None
    Total data:50
    Total data unique area:15


**Deskripsi statistik**


```python
# cek deskripsi statistik
df_canvassing.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>jumlah_petani_efishery</th>
      <th>total_jumlah_petani</th>
      <th>jumlah_kolam</th>
      <th>produksi_ikan</th>
      <th>penjualan_ikan</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>5.080000</td>
      <td>217.380000</td>
      <td>888.200000</td>
      <td>1723.900000</td>
      <td>831.900000</td>
    </tr>
    <tr>
      <th>std</th>
      <td>10.983921</td>
      <td>282.874378</td>
      <td>1195.352719</td>
      <td>7157.296711</td>
      <td>1410.348376</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>0.000000</td>
      <td>100.000000</td>
      <td>200.000000</td>
      <td>100.000000</td>
      <td>150.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>1.000000</td>
      <td>150.000000</td>
      <td>350.000000</td>
      <td>275.000000</td>
      <td>300.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>4.000000</td>
      <td>200.000000</td>
      <td>1000.000000</td>
      <td>500.000000</td>
      <td>950.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>50.000000</td>
      <td>1500.000000</td>
      <td>5000.000000</td>
      <td>50000.000000</td>
      <td>8000.000000</td>
    </tr>
  </tbody>
</table>
</div>



dari hasil deskripsi diatas, kita bisa tahu, ternyata tidak semua daerah ada petani ikannya. Selain itu, disini juga kita diperlihatkan ada daerah yang jumlah petaninya mencapai 1500. hmm, masih logis dibandingkan dengan data canvassing sales area sebelumnya yg sampai 12000. Tapi, bagaimanapun juga, nilai 1500 disini adalah hasil estimasi, jadi masih ada keraguan dalam kevalidan datanya.

Dari sini kita bisa ketahui bahwa ada:
- **15 Provinsi**
- **47 Kandidat Area**

Hal menarik yg ditemukan dalam data ini adalah:
- **>50% memiliki kesamaan dalam market leader**
- **Hampir 50% setiap area memiliki komoditas ikan utama yang sama**


Selanjutnya, kita akan mengubah semua kolom yg bukan numerik ke tipe categorical. Supaya bisa di encode


```python
#@title Konversi tipe data {display-mode: "form"}
df_canvassing_clean['kandidat_area'] = pd.Categorical(df_canvassing_clean['kandidat_area'])
df_canvassing_clean['provinsi'] = pd.Categorical(df_canvassing_clean['provinsi'])
df_canvassing_clean['fase_peluncuran'] = pd.Categorical(df_canvassing_clean['fase_peluncuran'])
df_canvassing_clean['komoditas_utama'] = pd.Categorical(df_canvassing_clean['komoditas_utama'])
df_canvassing_clean['market_leader_pakan'] = pd.Categorical(df_canvassing_clean['market_leader_pakan'])
```

```python
from sklearn.preprocessing import MultiLabelBinarizer

df_canvassing_clean = df_canvassing.copy()
mlb = MultiLabelBinarizer()
komoditas = mlb.fit_transform(
    df_canvassing_clean['komoditas_utama'].str.replace(
        'Nilem', 'Nila'
    ).str.replace(' ', '').str.split(',')
)
ikan = mlb.classes_
df_canvassing_clean = pd.concat([
    df_canvassing_clean,
    pd.DataFrame(komoditas, columns=ikan)
], axis=1)

mlb_pakan = MultiLabelBinarizer()
pakan = mlb_pakan.fit_transform(
    df_canvassing_clean['market_leader_pakan'].str.lower()..str.replace(' ', '').str.split(',')
)
merk_pakan = mlb_pakan.classes_
df_canvassing_clean = pd.concat([
    df_canvassing_clean,
    pd.DataFrame(pakan, columns=merk_pakan)
], axis=1)
```
Sekarang, mari kita lihat lagi **deskripsi statistiknya**.


```python
# deskripsi statistik
df_canvassing_clean.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>jumlah_petani_efishery</th>
      <th>total_jumlah_petani</th>
      <th>jumlah_kolam</th>
      <th>produksi_ikan</th>
      <th>penjualan_ikan</th>
      <th>Bandeng</th>
      <th>Bawal</th>
      <th>Gurame</th>
      <th>Lele</th>
      <th>Mas</th>
      <th>Nila</th>
      <th>Patin</th>
      <th>Udang</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.000000</td>
      <td>50.00000</td>
      <td>50.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>5.080000</td>
      <td>217.380000</td>
      <td>888.200000</td>
      <td>1723.900000</td>
      <td>831.900000</td>
      <td>0.020000</td>
      <td>0.060000</td>
      <td>0.260000</td>
      <td>0.420000</td>
      <td>0.280000</td>
      <td>0.620000</td>
      <td>0.30000</td>
      <td>0.040000</td>
    </tr>
    <tr>
      <th>std</th>
      <td>10.983921</td>
      <td>282.874378</td>
      <td>1195.352719</td>
      <td>7157.296711</td>
      <td>1410.348376</td>
      <td>0.141421</td>
      <td>0.239898</td>
      <td>0.443087</td>
      <td>0.498569</td>
      <td>0.453557</td>
      <td>0.490314</td>
      <td>0.46291</td>
      <td>0.197949</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.00000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>0.000000</td>
      <td>100.000000</td>
      <td>200.000000</td>
      <td>100.000000</td>
      <td>150.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.00000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>1.000000</td>
      <td>150.000000</td>
      <td>350.000000</td>
      <td>275.000000</td>
      <td>300.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.00000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>4.000000</td>
      <td>200.000000</td>
      <td>1000.000000</td>
      <td>500.000000</td>
      <td>950.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.750000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.00000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>50.000000</td>
      <td>1500.000000</td>
      <td>5000.000000</td>
      <td>50000.000000</td>
      <td>8000.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.00000</td>
      <td>1.000000</td>
    </tr>
  </tbody>
</table>
</div>



salah satu hal penting yg kita peroleh disini adalah, **lebih dari 50%** pelanggan efishery berada pada tahap **F2-F3**.

### Pengolahan Datanya
Selanjutnya kita akan mencari hubungan antar variabelnya. Tapi disini, kita akan melihatnya berdasarkan aggregasi per kota


```python
df_canvassing_clean['provinsi'].value_counts().plot.barh();
```


![png](images/output_24_0.png)



```python
df_canvassing_clean['fase_peluncuran'].value_counts().plot.pie();
```


![png](images/output_25_0.png)



```python
df_prov = df_canvassing_clean.groupby('provinsi').agg({
    'total_jumlah_petani': 'sum',
    'jumlah_petani_efishery': 'sum',
    'jumlah_kolam': 'sum',
    'produksi_ikan': 'sum',
    'penjualan_ikan': 'sum'
})
```

```python
fig, ax = plt.subplots(ncols=2, figsize=(20,7))
df_prov['total_jumlah_petani'].plot.barh(ax=ax[0]);
df_prov['jumlah_petani_efishery'].plot.barh(ax=ax[1])
ax[0].set_title('Total jumlah petani')
ax[1].set_title('Jumlah petani eFishery')
ax[0].legend('')
ax[1].legend('');
```


![png](images/output_27_0.png)



```python
df_prov['density_petani_efishery'] = df_prov['jumlah_petani_efishery'] / df_prov['total_jumlah_petani']
df_prov['density_kolam_petani'] = df_prov['jumlah_kolam'] / df_prov['total_jumlah_petani']
df_prov['density_produksi_ikan'] = df_prov['produksi_ikan'] / df_prov['jumlah_kolam']
df_prov['density_penjualan'] = df_prov['penjualan_ikan'] / df_prov['jumlah_kolam']
```

```python
density_cols = [
    'density_petani_efishery',
    'density_kolam_petani',
    'density_produksi_ikan',
    'density_penjualan'
]

for col in density_cols:
    fig, ax = plt.subplots()
    df_prov[col].plot.barh(ax=ax)
    ax.set_title(col);
```


![png](images/output_29_0.png)




![png](images/output_29_1.png)




![png](images/output_29_2.png)




![png](images/output_29_3.png)


Dari sini sudah terlihat jelas kalau fase **F2-F3** adalah fase yg dominan dari pelanggan efishery (64%). Selain itu, disini juga kita bisa tahu kalau hampir sebagian besar provinsi memiliki jumlah petani efishery kurang dari 20, padahal rata-rata jumlah petani disetiap provinsinya sekitar 700, dan juga sebagian provinsi tersebut memiliki jumlah kolam dibawah 4000.

Disini juga ada fitur baru, yaitu `density` yg digunakan untuk mengukur kepadatan kolam per petani, petani efishery per petani total, serta kepadatan produksi/penjualan ikan per kolam.

Dari density yg sudah dihitung, ternyata rasio terbesar petani efishery terhadap total jumlah petani di suatu provinsi tidak sampai 30%, selain itu dari data diatas juga dapat dilihat kalau masih banyak daerah yang blm ada petani efisherynya. Sehingga masih besar peluang efishery untuk menjangkau lebih banyak petani lagi.

Selanjutnya kita akan cek lagi hubungan antar variabelnya.


```python
sns.heatmap(
    df_prov.iloc[:,:5].corr(),
    center=0,
    annot=True,
    fmt='.2f',
    square=True
);
```


![png](images/output_31_0.png)


Dari sini kita bisa tahu kalau semakin banyak jumlah petani, semakin banyak jomlah kolam dan penjualan ikannya. Namun disini kita bisa tahu ternyata hubungan jumlah kolam dan produksi ikan itu tidak terlalu tinggi nilainya, wahh itu kenapa ya?, apa karena belum efisien proses budidayanya?


```python
sns.pairplot(df_prov.iloc[~df_prov.index.isin(['Jabar', 'Jateng', 'Jatim']),:5], kind='reg');
```


![png](images/output_33_0.png)


Dari sini kita bisa tahu ternyata masih ada daerah yg petaninya banyak tapi yg jadi **client efishery** sedikit. Selain itu, ternyata banyaknya jumlah kolam tidak menjamin **produksi** ikannya besar. Dan ternyata masih ada yg penjualan ikannya **belum efisien**, yg ditandai jumlah produksinya besar, tapi penjualannya kecil.

Selanjutnya kita akan cek, keterkaitannya dengan jumlah kandidat area efishery.


```python
df_canvassing_clean[['provinsi'] + ikan.tolist()].groupby('provinsi').sum().style.background_gradient()
```




<style  type="text/css" >
    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col3 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col5 {
            background-color:  #e0dded;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col3 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col5 {
            background-color:  #f1ebf5;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col2 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col3 {
            background-color:  #a5bddb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col4 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col5 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col6 {
            background-color:  #056faf;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col3 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col5 {
            background-color:  #f1ebf5;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col6 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col3 {
            background-color:  #a5bddb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col5 {
            background-color:  #c9cee4;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col0 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col2 {
            background-color:  #046198;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col3 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col5 {
            background-color:  #f1ebf5;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col6 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col7 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col1 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col5 {
            background-color:  #c9cee4;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col6 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col6 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col2 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col3 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col5 {
            background-color:  #e0dded;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col6 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col5 {
            background-color:  #c9cee4;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col7 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col3 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col5 {
            background-color:  #f1ebf5;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col6 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col4 {
            background-color:  #f1ebf5;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col5 {
            background-color:  #f1ebf5;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col4 {
            background-color:  #e0dded;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col5 {
            background-color:  #e0dded;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col3 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col6 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }</style><table id="T_b64717e8_afa2_11ea_8f07_70bc1070a804" ><thead>    <tr>        <th class="blank level0" ></th>        <th class="col_heading level0 col0" >Bandeng</th>        <th class="col_heading level0 col1" >Bawal</th>        <th class="col_heading level0 col2" >Gurame</th>        <th class="col_heading level0 col3" >Lele</th>        <th class="col_heading level0 col4" >Mas</th>        <th class="col_heading level0 col5" >Nila</th>        <th class="col_heading level0 col6" >Patin</th>        <th class="col_heading level0 col7" >Udang</th>    </tr>    <tr>        <th class="index_name level0" >provinsi</th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>    </tr></thead><tbody>
                <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row0" class="row_heading level0 row0" >Bali</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col0" class="data row0 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col1" class="data row0 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col2" class="data row0 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col3" class="data row0 col3" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col4" class="data row0 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col5" class="data row0 col5" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col6" class="data row0 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row0_col7" class="data row0 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row1" class="row_heading level0 row1" >Bengkulu</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col0" class="data row1 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col1" class="data row1 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col2" class="data row1 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col3" class="data row1 col3" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col4" class="data row1 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col5" class="data row1 col5" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col6" class="data row1 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row1_col7" class="data row1 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row2" class="row_heading level0 row2" >DIY</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col0" class="data row2 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col1" class="data row2 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col2" class="data row2 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col3" class="data row2 col3" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col4" class="data row2 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col5" class="data row2 col5" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col6" class="data row2 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row2_col7" class="data row2 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row3" class="row_heading level0 row3" >Jabar</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col0" class="data row3 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col1" class="data row3 col1" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col2" class="data row3 col2" >6</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col3" class="data row3 col3" >3</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col4" class="data row3 col4" >11</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col5" class="data row3 col5" >11</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col6" class="data row3 col6" >3</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row3_col7" class="data row3 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row4" class="row_heading level0 row4" >Jambi</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col0" class="data row4 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col1" class="data row4 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col2" class="data row4 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col3" class="data row4 col3" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col4" class="data row4 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col5" class="data row4 col5" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col6" class="data row4 col6" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row4_col7" class="data row4 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row5" class="row_heading level0 row5" >Jateng</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col0" class="data row5 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col1" class="data row5 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col2" class="data row5 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col3" class="data row5 col3" >3</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col4" class="data row5 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col5" class="data row5 col5" >3</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col6" class="data row5 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row5_col7" class="data row5 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row6" class="row_heading level0 row6" >Jatim</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col0" class="data row6 col0" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col1" class="data row6 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col2" class="data row6 col2" >5</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col3" class="data row6 col3" >8</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col4" class="data row6 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col5" class="data row6 col5" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col6" class="data row6 col6" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row6_col7" class="data row6 col7" >1</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row7" class="row_heading level0 row7" >Kalsel</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col0" class="data row7 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col1" class="data row7 col1" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col2" class="data row7 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col3" class="data row7 col3" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col4" class="data row7 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col5" class="data row7 col5" >3</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col6" class="data row7 col6" >4</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row7_col7" class="data row7 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row8" class="row_heading level0 row8" >Kalteng</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col0" class="data row8 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col1" class="data row8 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col2" class="data row8 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col3" class="data row8 col3" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col4" class="data row8 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col5" class="data row8 col5" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col6" class="data row8 col6" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row8_col7" class="data row8 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row9" class="row_heading level0 row9" >Lampung</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col0" class="data row9 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col1" class="data row9 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col2" class="data row9 col2" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col3" class="data row9 col3" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col4" class="data row9 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col5" class="data row9 col5" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col6" class="data row9 col6" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row9_col7" class="data row9 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row10" class="row_heading level0 row10" >NTB</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col0" class="data row10 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col1" class="data row10 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col2" class="data row10 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col3" class="data row10 col3" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col4" class="data row10 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col5" class="data row10 col5" >3</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col6" class="data row10 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row10_col7" class="data row10 col7" >1</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row11" class="row_heading level0 row11" >Riau</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col0" class="data row11 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col1" class="data row11 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col2" class="data row11 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col3" class="data row11 col3" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col4" class="data row11 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col5" class="data row11 col5" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col6" class="data row11 col6" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row11_col7" class="data row11 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row12" class="row_heading level0 row12" >Sulut</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col0" class="data row12 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col1" class="data row12 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col2" class="data row12 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col3" class="data row12 col3" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col4" class="data row12 col4" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col5" class="data row12 col5" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col6" class="data row12 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row12_col7" class="data row12 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row13" class="row_heading level0 row13" >Sumbar</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col0" class="data row13 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col1" class="data row13 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col2" class="data row13 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col3" class="data row13 col3" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col4" class="data row13 col4" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col5" class="data row13 col5" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col6" class="data row13 col6" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row13_col7" class="data row13 col7" >0</td>
            </tr>
            <tr>
                        <th id="T_b64717e8_afa2_11ea_8f07_70bc1070a804level0_row14" class="row_heading level0 row14" >Sumsel</th>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col0" class="data row14 col0" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col1" class="data row14 col1" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col2" class="data row14 col2" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col3" class="data row14 col3" >1</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col4" class="data row14 col4" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col5" class="data row14 col5" >0</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col6" class="data row14 col6" >2</td>
                        <td id="T_b64717e8_afa2_11ea_8f07_70bc1070a804row14_col7" class="data row14 col7" >0</td>
            </tr>
    </tbody></table>




```python
df_canvassing_clean[
    ['provinsi'] + merk_pakan.tolist()
].groupby('provinsi').sum().style.background_gradient(axis=1)
```




<style  type="text/css" >
    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col0 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col3 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col9 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col10 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col3 {
            background-color:  #358fc0;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col7 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col8 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col9 {
            background-color:  #056faf;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col10 {
            background-color:  #358fc0;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col11 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col12 {
            background-color:  #ece7f2;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col2 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col3 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col2 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col3 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col6 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col7 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col10 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col0 {
            background-color:  #d7d6e9;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col3 {
            background-color:  #d7d6e9;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col5 {
            background-color:  #eee9f3;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col6 {
            background-color:  #eee9f3;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col7 {
            background-color:  #8bb2d4;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col9 {
            background-color:  #589ec8;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col10 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col1 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col2 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col9 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col10 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col1 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col2 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col10 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col1 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col2 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col3 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col7 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col9 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col10 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col11 {
            background-color:  #b4c4df;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col0 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col1 {
            background-color:  #056faf;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col3 {
            background-color:  #056faf;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col4 {
            background-color:  #d0d1e6;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col2 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col3 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col1 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col3 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col7 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col10 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col0 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col1 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col2 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col3 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col4 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col5 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col6 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col7 {
            background-color:  #73a9cf;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col8 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col9 {
            background-color:  #023858;
            color:  #f1f1f1;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col10 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col11 {
            background-color:  #fff7fb;
            color:  #000000;
        }    #T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col12 {
            background-color:  #fff7fb;
            color:  #000000;
        }</style><table id="T_50273aa0_afa3_11ea_8f07_70bc1070a804" ><thead>    <tr>        <th class="blank level0" ></th>        <th class="col_heading level0 col0" >CJFeed(Samsung)</th>        <th class="col_heading level0 col1" >CPP</th>        <th class="col_heading level0 col2" >Cargill</th>        <th class="col_heading level0 col3" >Global</th>        <th class="col_heading level0 col4" >Goldcoin</th>        <th class="col_heading level0 col5" >Haida</th>        <th class="col_heading level0 col6" >Havindo</th>        <th class="col_heading level0 col7" >MatahariSakti</th>        <th class="col_heading level0 col8" >SInta</th>        <th class="col_heading level0 col9" >STP</th>        <th class="col_heading level0 col10" >Sinta</th>        <th class="col_heading level0 col11" >TongWei</th>        <th class="col_heading level0 col12" >Wonokoyo</th>    </tr>    <tr>        <th class="index_name level0" >provinsi</th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>        <th class="blank" ></th>    </tr></thead><tbody>
                <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row0" class="row_heading level0 row0" >Bali</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col0" class="data row0 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col1" class="data row0 col1" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col2" class="data row0 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col3" class="data row0 col3" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col4" class="data row0 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col5" class="data row0 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col6" class="data row0 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col7" class="data row0 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col8" class="data row0 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col9" class="data row0 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col10" class="data row0 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col11" class="data row0 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row0_col12" class="data row0 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row1" class="row_heading level0 row1" >Bengkulu</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col0" class="data row1 col0" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col1" class="data row1 col1" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col2" class="data row1 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col3" class="data row1 col3" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col4" class="data row1 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col5" class="data row1 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col6" class="data row1 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col7" class="data row1 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col8" class="data row1 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col9" class="data row1 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col10" class="data row1 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col11" class="data row1 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row1_col12" class="data row1 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row2" class="row_heading level0 row2" >DIY</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col0" class="data row2 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col1" class="data row2 col1" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col2" class="data row2 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col3" class="data row2 col3" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col4" class="data row2 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col5" class="data row2 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col6" class="data row2 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col7" class="data row2 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col8" class="data row2 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col9" class="data row2 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col10" class="data row2 col10" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col11" class="data row2 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row2_col12" class="data row2 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row3" class="row_heading level0 row3" >Jabar</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col0" class="data row3 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col1" class="data row3 col1" >8</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col2" class="data row3 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col3" class="data row3 col3" >5</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col4" class="data row3 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col5" class="data row3 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col6" class="data row3 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col7" class="data row3 col7" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col8" class="data row3 col8" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col9" class="data row3 col9" >6</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col10" class="data row3 col10" >5</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col11" class="data row3 col11" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row3_col12" class="data row3 col12" >1</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row4" class="row_heading level0 row4" >Jambi</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col0" class="data row4 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col1" class="data row4 col1" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col2" class="data row4 col2" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col3" class="data row4 col3" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col4" class="data row4 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col5" class="data row4 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col6" class="data row4 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col7" class="data row4 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col8" class="data row4 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col9" class="data row4 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col10" class="data row4 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col11" class="data row4 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row4_col12" class="data row4 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row5" class="row_heading level0 row5" >Jateng</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col0" class="data row5 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col1" class="data row5 col1" >4</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col2" class="data row5 col2" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col3" class="data row5 col3" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col4" class="data row5 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col5" class="data row5 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col6" class="data row5 col6" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col7" class="data row5 col7" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col8" class="data row5 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col9" class="data row5 col9" >4</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col10" class="data row5 col10" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col11" class="data row5 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row5_col12" class="data row5 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row6" class="row_heading level0 row6" >Jatim</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col0" class="data row6 col0" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col1" class="data row6 col1" >9</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col2" class="data row6 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col3" class="data row6 col3" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col4" class="data row6 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col5" class="data row6 col5" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col6" class="data row6 col6" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col7" class="data row6 col7" >4</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col8" class="data row6 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col9" class="data row6 col9" >5</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col10" class="data row6 col10" >3</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col11" class="data row6 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row6_col12" class="data row6 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row7" class="row_heading level0 row7" >Kalsel</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col0" class="data row7 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col1" class="data row7 col1" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col2" class="data row7 col2" >4</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col3" class="data row7 col3" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col4" class="data row7 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col5" class="data row7 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col6" class="data row7 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col7" class="data row7 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col8" class="data row7 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col9" class="data row7 col9" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col10" class="data row7 col10" >4</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col11" class="data row7 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row7_col12" class="data row7 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row8" class="row_heading level0 row8" >Kalteng</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col0" class="data row8 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col1" class="data row8 col1" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col2" class="data row8 col2" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col3" class="data row8 col3" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col4" class="data row8 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col5" class="data row8 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col6" class="data row8 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col7" class="data row8 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col8" class="data row8 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col9" class="data row8 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col10" class="data row8 col10" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col11" class="data row8 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row8_col12" class="data row8 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row9" class="row_heading level0 row9" >Lampung</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col0" class="data row9 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col1" class="data row9 col1" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col2" class="data row9 col2" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col3" class="data row9 col3" >3</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col4" class="data row9 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col5" class="data row9 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col6" class="data row9 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col7" class="data row9 col7" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col8" class="data row9 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col9" class="data row9 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col10" class="data row9 col10" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col11" class="data row9 col11" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row9_col12" class="data row9 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row10" class="row_heading level0 row10" >NTB</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col0" class="data row10 col0" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col1" class="data row10 col1" >3</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col2" class="data row10 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col3" class="data row10 col3" >3</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col4" class="data row10 col4" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col5" class="data row10 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col6" class="data row10 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col7" class="data row10 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col8" class="data row10 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col9" class="data row10 col9" >4</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col10" class="data row10 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col11" class="data row10 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row10_col12" class="data row10 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row11" class="row_heading level0 row11" >Riau</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col0" class="data row11 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col1" class="data row11 col1" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col2" class="data row11 col2" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col3" class="data row11 col3" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col4" class="data row11 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col5" class="data row11 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col6" class="data row11 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col7" class="data row11 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col8" class="data row11 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col9" class="data row11 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col10" class="data row11 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col11" class="data row11 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row11_col12" class="data row11 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row12" class="row_heading level0 row12" >Sulut</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col0" class="data row12 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col1" class="data row12 col1" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col2" class="data row12 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col3" class="data row12 col3" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col4" class="data row12 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col5" class="data row12 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col6" class="data row12 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col7" class="data row12 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col8" class="data row12 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col9" class="data row12 col9" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col10" class="data row12 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col11" class="data row12 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row12_col12" class="data row12 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row13" class="row_heading level0 row13" >Sumbar</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col0" class="data row13 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col1" class="data row13 col1" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col2" class="data row13 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col3" class="data row13 col3" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col4" class="data row13 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col5" class="data row13 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col6" class="data row13 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col7" class="data row13 col7" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col8" class="data row13 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col9" class="data row13 col9" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col10" class="data row13 col10" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col11" class="data row13 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row13_col12" class="data row13 col12" >0</td>
            </tr>
            <tr>
                        <th id="T_50273aa0_afa3_11ea_8f07_70bc1070a804level0_row14" class="row_heading level0 row14" >Sumsel</th>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col0" class="data row14 col0" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col1" class="data row14 col1" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col2" class="data row14 col2" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col3" class="data row14 col3" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col4" class="data row14 col4" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col5" class="data row14 col5" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col6" class="data row14 col6" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col7" class="data row14 col7" >1</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col8" class="data row14 col8" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col9" class="data row14 col9" >2</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col10" class="data row14 col10" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col11" class="data row14 col11" >0</td>
                        <td id="T_50273aa0_afa3_11ea_8f07_70bc1070a804row14_col12" class="data row14 col12" >0</td>
            </tr>
    </tbody></table>




```python
from spacy.lang import id

nlp = id.Indonesian()

def tokenizer(text):
    return [token.lemma_ for token in nlp(text)]
```

```python
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.manifold import TSNE
from sklearn.pipeline import make_pipeline
import altair as alt

pipe = make_pipeline(
    CountVectorizer(tokenizer=tokenizer),
    TruncatedSVD(2, random_state=42)
)
X = pipe.fit_transform(df_canvassing_clean['alasan_dianjurkan'])
```

```python
alt.Chart(pd.DataFrame({
    'x0': X[:,0],
    'x1': X[:,1],
    'text': df_canvassing_clean['alasan_dianjurkan'],
    'provinsi': df_canvassing_clean['provinsi'],
    'efishery': df_canvassing_clean['jumlah_petani_efishery']
})).mark_circle().encode(
    x='x0',
    y='x1',
    color='provinsi',
    tooltip='text'
).properties(
    width=500,
    height=500
).interactive()
```





<div id="altair-viz-57cc9a4845c2436cacd63b87921a2456"></div>
<script type="text/javascript">
  (function(spec, embedOpt){
    let outputDiv = document.currentScript.previousElementSibling;
    if (outputDiv.id !== "altair-viz-57cc9a4845c2436cacd63b87921a2456") {
      outputDiv = document.getElementById("altair-viz-57cc9a4845c2436cacd63b87921a2456");
    }
    const paths = {
      "vega": "https://cdn.jsdelivr.net/npm//vega@5?noext",
      "vega-lib": "https://cdn.jsdelivr.net/npm//vega-lib?noext",
      "vega-lite": "https://cdn.jsdelivr.net/npm//vega-lite@4.8.1?noext",
      "vega-embed": "https://cdn.jsdelivr.net/npm//vega-embed@6?noext",
    };

    function loadScript(lib) {
      return new Promise(function(resolve, reject) {
        var s = document.createElement('script');
        s.src = paths[lib];
        s.async = true;
        s.onload = () => resolve(paths[lib]);
        s.onerror = () => reject(`Error loading script: ${paths[lib]}`);
        document.getElementsByTagName("head")[0].appendChild(s);
      });
    }

    function showError(err) {
      outputDiv.innerHTML = `<div class="error" style="color:red;">${err}</div>`;
      throw err;
    }

    function displayChart(vegaEmbed) {
      vegaEmbed(outputDiv, spec, embedOpt)
        .catch(err => showError(`Javascript Error: ${err.message}<br>This usually means there's a typo in your chart specification. See the javascript console for the full traceback.`));
    }

    if(typeof define === "function" && define.amd) {
      requirejs.config({paths});
      require(["vega-embed"], displayChart, err => showError(`Error loading script: ${err.message}`));
    } else if (typeof vegaEmbed === "function") {
      displayChart(vegaEmbed);
    } else {
      loadScript("vega")
        .then(() => loadScript("vega-lite"))
        .then(() => loadScript("vega-embed"))
        .catch(showError)
        .then(() => displayChart(vegaEmbed));
    }
  })({"config": {"view": {"continuousWidth": 400, "continuousHeight": 300}}, "data": {"name": "data-aaef2264094118a121ab58a5b7a012ad"}, "mark": "circle", "encoding": {"color": {"type": "nominal", "field": "provinsi"}, "tooltip": {"type": "nominal", "field": "text"}, "x": {"type": "quantitative", "field": "x0"}, "y": {"type": "quantitative", "field": "x1"}}, "height": 500, "selection": {"selector008": {"type": "interval", "bind": "scales", "encodings": ["x", "y"]}}, "width": 500, "$schema": "https://vega.github.io/schema/vega-lite/v4.8.1.json", "datasets": {"data-aaef2264094118a121ab58a5b7a012ad": [{"x0": 0.5952192678734295, "x1": 0.11820180461667733, "text": "Sudah melakukan penjajakan dari awal bulan Januari 2020 untuk rencana pembentukan KPD Nila di narmada", "provinsi": "NTB", "efishery": 4}, {"x0": 5.417471405909365, "x1": -2.237226687148192, "text": "Garut termasuk yg perikanannya cukup besar setelah saya diskusi dgn para penyuluh perikanan tasik. Wilayah paling besar adalah di wilayah karangpawitan, wanaraja dan sukawening. Jumlah kolam yg sudah masuk data canvasing sekitar 800 kolam dan 150 petani. Ini yg menjadi salah satu konsen saya untuk wilayah garut.", "provinsi": "Jabar", "efishery": 1}, {"x0": 1.9412813745587654, "x1": -0.6228028958509102, "text": "Berpotensi untuk pengembangan pakan dan fedeer. Beberapa petani sudah mulai ada kelompok tani.", "provinsi": "Jatim", "efishery": 2}, {"x0": 1.1888968974544132, "x1": 0.7095707335408614, "text": "Merupakan salah satu kawasan merupakan sentra minapolitan ikan konsumsi (Lele, Patin dan Gurame)", "provinsi": "Jabar", "efishery": 4}, {"x0": 1.440312322510669, "x1": -0.513025175704454, "text": "Masih banyak ada pertumbuhan kolam yang baru dan juga para pembudidaya yang menggunakan pakan buatan dan diharapkan setelah ada point bisa membantu pembudidaya dalam masalah funding juga masalah jual hasil nya", "provinsi": "Kalsel", "efishery": 26}, {"x0": 0.8795446421505942, "x1": -0.4155351784914753, "text": "Daerah sentra patin dan masih banyak yang menggunakan pakan mandiri jadi dengan bikin point f2/f3 bisa menggantikan pakan mandiri ke pakan pabrikan", "provinsi": "Sumsel", "efishery": 30}, {"x0": 19.69111251964965, "x1": 9.293175448515155, "text": "Potensi ikan sangat besar di pati (sesuai slogan kab. Pati adalah Bumi Mina Tani) yg tersebar di kec. Ngantru, Tayu, Juwana dan Dukuhseti. Dimana, sentra ikan nila ada di Tayu dan sebagian di dukuhseti, sementara utk ikan lele ada di ngantru dan juwana (desa bakaran wetan/kulon).\nLahan produktif saat ini utk Nila sbg contoh di kec. Tayu ada sekitar 715 Ha, namun yg jadi kendala saat ini jaringan listrik baru mengcover sktr 5% nya saja (masih tunggu bantuan dinas). Saat kondisi normal panen ikan nila di kec. Tayu bisa mencapai 4 ton/ hari. \nUtk komoditi lain yaitu lele di kec. Ngantru dan Juwana, sebagian sdh teraliri jaringan listrik. Luas lahan produktif kolam lele lebih dr 100 Ha (Juwana dan Ngantru).", "provinsi": "Jateng", "efishery": 5}, {"x0": 1.32824049971625, "x1": -0.5773202191958383, "text": "Potensi cukup besar, petani belum bayak tersentuh teknology. Fase 1 karena baru mulai pengenalan", "provinsi": "Sumbar", "efishery": 2}, {"x0": 3.6299730427908714, "x1": 2.3276881840578953, "text": "Agen pakan &Komunitas petani gurami & lele ,terutama  gurami lumayan banyak , distribusi hasl ikan nya jg cmn satu tempat yaitu di Bali ,kemungkinan bisa di ajak kerjasama agen nya (Suryadi + Sarmidi) ,mas Danang ,pak Hanif ,pak dadit jg sdh pernah ngobrol sewaktu ada sarasehan kmren", "provinsi": "Jatim", "efishery": 2}, {"x0": 5.488086887755242, "x1": 1.0214203210098156, "text": "area ini sudah ter deploy autofeeder efishery, dan penerimaannya sangat baik. autofeeder sudah terbukti mampu meningkatkan produksi nila. namun, penjualan feeder di daerah ini terkendala market ikan nila. apabila kita mampu membantu market ikan nila di Bengkulu maka kita mampu meningkatkan penetrasi penjualan feeder di Argamakmur", "provinsi": "Bengkulu", "efishery": 50}, {"x0": 0.26820300150431353, "x1": -0.004556846230083797, "text": "Merupakan sentral budidaya nila untuk daerah Lombok timur", "provinsi": "NTB", "efishery": 2}, {"x0": 8.532310889783119, "x1": -3.887257903709152, "text": "Area ciamis (kab. Pangandaran) potensi area perikanan yg cukup besar hampir sama dgn area tasikmalaya. Karrna centra ikan nila dan gurame ada di 2 lokasi tersebut. Kenapa F1 yg di pilih karena minat mereka yg tertarik dan sudah tau teknologi efishery hanya belum ada contoh yg berhasil di area ciamis. Selain dr F1 ciamis memerlukan F2 di karenakan banyak petani tradisional yg ingin bekembang ke budidaya intensive yg full pelet hanya keterbatasan dr modal.", "provinsi": "Jabar", "efishery": 1}, {"x0": 1.7060431243755438, "x1": -0.16104135890652732, "text": "Petani pembudidaya pembenihan dan pembesaran lele konsumsi. Dan ada salah satu key customer seperti republik lele", "provinsi": "Jatim", "efishery": 2}, {"x0": 1.2297473289218013, "x1": -0.2921572856021566, "text": "Merupakan salah satu area perikanan KJA di wilayah purwakarta.", "provinsi": "Jabar", "efishery": 0}, {"x0": 1.7355251457592178, "x1": 0.38716477899448104, "text": "Area berdekatan dengan Banjarbaru, jadi ya baiknya di jadikan point juga (satu point untuk 2 kota yang berdekatan dan di martapura banyak nya pembibitan", "provinsi": "Kalsel", "efishery": 8}, {"x0": 2.1721037241884624, "x1": -1.1784964400859455, "text": "Karena dikampar sudah ada integrated cold storage program dari bupati kampar. Tapi karena bupatinya meninggal ics tersebut terbengkalai dan sekarang mulai dihidupkan oleh swasta. Kampar merupakan sentra perikanan yang besar driau", "provinsi": "Riau", "efishery": 10}, {"x0": 8.818962001506893, "x1": -2.813564681702965, "text": "Boyolali terkenal dg kampung lelenya yg meliputi kec. teras dan kec. sawit. Ribuan kolam lele masih produktif. \nTotal produksi per bulan bisa mencapai 500 ton ikan. \nPerputaran pakan di area tersebut sangat besar dan setidaknya ada 3 agen yg cukup terkenal dan mendominasi : daryanto, ernawan dan h.alip. \nHarga pakan variasi mengikuti tempo mingguan yg diberlakukan ke petani. \n", "provinsi": "Jateng", "efishery": 0}, {"x0": 1.3120972939710347, "x1": -0.548628417772006, "text": "Potensi cukup besar, belum banyak tersentuh teknology. Fase 1 karena baru akan dimulai", "provinsi": "Sulut", "efishery": 0}, {"x0": 1.1291565378770805, "x1": 0.1244200875044821, "text": "Jumlah petani lumayan banyak, dan bisa untuk di ajak bekerjasama", "provinsi": "Jatim", "efishery": 1}, {"x0": 0.2457809689353197, "x1": -0.012884254687561117, "text": "Sentral budidaya nila untuk daerah Lombok tengah", "provinsi": "NTB", "efishery": 0}, {"x0": 2.7243441734016303, "x1": -1.7953545288201591, "text": "Area ini lebih cocok ketika F2 dahulu yg masuk untuk menunjang teknologi efishery nya masuk.\nyg jd alasan mendasar harus f2 dulu yg masuk dikarenakan jaringan listrik untuk wilayah jati gede belum tersedia.", "provinsi": "Jabar", "efishery": 0}, {"x0": 1.5332759624752306, "x1": -0.07626810758960115, "text": "Komunitas kelompok tani nya setiap bulan selalu mengadakan acara di dinas perikanan ,sudah pernah sarasehan disana. ", "provinsi": "Jateng", "efishery": 5}, {"x0": 1.701939944685683, "x1": -0.4306329950982302, "text": "Pernah menjadi area perikanan, hanya sekarang sudah banyak beralih ke pembenihan dan sentra ikan hias.", "provinsi": "Jabar", "efishery": 1}, {"x0": 1.8492885980021623, "x1": -0.9254343112708397, "text": "Area tersebut banyak pembudiaya2 kecil, dan potensi untuk pakan sangat besar karena produksinya ke 2 setelah banjarbaru, namun tidak cocok untuk Auto feeder karena kolam2 jauh dari sumber listrik", "provinsi": "Kalsel", "efishery": 7}, {"x0": 2.4391449975480275, "x1": -0.5552633930596717, "text": "Muaro jambi cukup potensial banyak kolam jaring apung sepanjang sungai batanghari. Dikolam darat budidaya ikan patin dan ikan lele juga besar. Strategi masuk ke jambi dengan program pakai feeder dibantu pakan", "provinsi": "Jambi", "efishery": 0}, {"x0": 9.55707300389521, "x1": -7.646353669855095, "text": "Kab. Sleman memiliki potensi perikanan yg cukup besar dan tersebar hampir merata di bbrp kecamatan. Produksi mayoritas pembibitan namun mulai beralih ke pembesaran. \nDinas perikanan sleman cukup aktif dalam membina kelompok pembudidaya di sleman dan ada agenda rutin rapat per 3 bulan antara dinas dan pokdakan se kab. Sleman, shg apabila efishery mampu mengawali kerjasama dg dinas perikanan sleman dalam hal perikanan berteknologi akan sangat potensial sekali karena mayoritas pokdakan sangat bergantung dg kebijakan dinas.\nBeberapa pokdakan memiliki fasilitas yg cukup komplit dari mulai tempat panen, koperasi dan sistem pembukuan yg rapi. ", "provinsi": "DIY", "efishery": 0}, {"x0": 0.1594231839684294, "x1": -0.08935162873392337, "text": "Petani membutuhkan bantuan pendanaan pengadaan pakan", "provinsi": "Jabar", "efishery": 0}, {"x0": 0.7115303315031465, "x1": -0.5776311688890512, "text": "Ada bbrp agen pakan yg jg mempunyai banyak kolam & komunitas petani lumayan banyak", "provinsi": "Jatim", "efishery": 2}, {"x0": 1.9891269602213844, "x1": -1.4099726279794154, "text": "Karena bisa bekerjasama Dengan agen STP yg juga sekaligus agen pengadaan kolam bundar, dimana agen tersebut sudah memegang jumlah petani yg cukup banyak. Mulai dari penyediaan benih sampai panen", "provinsi": "Bali", "efishery": 0}, {"x0": 0.38166039043451594, "x1": -0.398986932358408, "text": "Ketersediaan lahan produktif yg menunjang bididaya perikanan", "provinsi": "Jabar", "efishery": 0}, {"x0": 2.5972231247172517, "x1": -0.39293743538009296, "text": "Banyak pembudidaya gurameh dan lele yang notabene dekat dengan sentra perikanan Jatim yaitu Tulungagung. Dan sudah pernah acara sarasehan di daerah tsb.", "provinsi": "Jatim", "efishery": 1}, {"x0": 1.1457344993308747, "x1": -0.4894217333718692, "text": "Terdapat salah satu sentra perikanan KJA dan beberapa lokasi kolam air deras.", "provinsi": "Jabar", "efishery": 0}, {"x0": 1.7163360487410706, "x1": -0.11114025574764948, "text": "Potensi untuk pakan dan ikan cukup lumayan besar, namun untuk feeder masib kurang karena kolam jauh dari pemukinan (kendala listrik)", "provinsi": "Kalsel", "efishery": 4}, {"x0": 2.923730588356987, "x1": -0.35705609736688315, "text": "Program pakai feeder dibantu pakan. Sudah ada customer efishery bp idman dbengkulu selatan. Sempat kesulitan pakan karena pemasaran ikan sulit. Yang sangat dbtuhkan adalah pemasaran ikan nila", "provinsi": "Bengkulu", "efishery": 5}, {"x0": 8.925404778691727, "x1": -2.1342328487565285, "text": "Mayoritas komoditi ikan nila ada di kec. Tulung, polanharjo dan karanganom, rata2 kolam semen untuk pembesaran. Pernah ada trial autofeeder namun tidak berlanjut karena faktor maintainance yg loss. \nUntuk pemasaran hasil panen tidak terlalu sulit karena nila asal klaten memiliki kualitas yg bagus. \nPonggok sendiri daerah tujuan wisata, shg jika perikanan di daerah ini didukung dg teknologi akan sangat menarik namun ketertarikan terhadap feeder masih rendah dikarenakan biaya produksi dan sewa lahan cukup mahal, shg perlu support lain mungkin dg bantuan permodalan pakan spy lebih menarik minat petani/ pembudidaya. ", "provinsi": "Jateng", "efishery": 0}, {"x0": 0.15927197072516072, "x1": -0.08892296467595034, "text": "Petani membutuhkan bantuan pengadaan pakan", "provinsi": "Jabar", "efishery": 0}, {"x0": 1.6579997824551975, "x1": -0.28446260097570364, "text": "Ada bbrp petani yg kolam nya kecil tetapi dlm jmlh banyak & kolam buat benih , dan menyarankan untuk AF mini , kemungkinan bisa diajak kerjasama", "provinsi": "Jatim", "efishery": 3}, {"x0": 0.4587225257131156, "x1": -0.7178769502730631, "text": "Cukup potensial untuk penjualan yg dibanding dengan pakan", "provinsi": "NTB", "efishery": 1}, {"x0": 1.9354590656227793, "x1": -0.8159507480732173, "text": "F1 lokasi keramba bisa teraliri listrik langsung dr rumah terdekat, karena jarak rumah  pembudidaya dgn keramba tidak jauh.\nF2 dan F3 juga masuk karena melihat potensi penggunan pakan pelet", "provinsi": "Jabar", "efishery": 0}, {"x0": 3.0393764693382086, "x1": -0.04258892117859505, "text": "Potensi perikanan Madiun terus tumbuh . Khususnya di budidaya lele. Ada beberapa key customer seperti Ajis dan pak mat.notabene budidaya di lele dan juga supplier ", "provinsi": "Jatim", "efishery": 1}, {"x0": 0.3094991746594214, "x1": -0.011955355482620673, "text": "Salah satu sentra perikanan khususnya ikan mas", "provinsi": "Jabar", "efishery": 2}, {"x0": 0.76287536327003, "x1": -0.05675519909381622, "text": "daerah kalteng banyaknya keramba, namun untuk daerah kapuas & pulang piso ada beberapa kolam ", "provinsi": "Kalteng", "efishery": 2}, {"x0": 0.9546911003339696, "x1": -0.4642054543347993, "text": "Sudah masuk beberapa feeder disana dan sangat dibutuhkan tim yang bisa membantu budidaya dan penerapan teknologi juga", "provinsi": "Sumbar", "efishery": 15}, {"x0": 7.1380313899472085, "x1": -2.955928551534517, "text": "Sample feeder efishery pernah diberikan kepada salah seorang pembudidaya yg memiliki KJA banyak an Gunanto di -desa Bulu- sebanyak 4 unit tapi tidak dipakai karena berbenturan dg tenaga kerja serta feeding program versi mereka. \nNamum demikian, potensi serapan pakan di area tersebut sngat besar. Shg jika bisnis unit (feedfun) maju terlebih dahulu bisa merubah skema utk pemakaian teknologi autofeeder. \nNote : perlu detail data (potensi yg tertulis baru lingkup daerah bulu, smntr ada 2 daerah lagi yg punya potensi KJA yg masuk area kedung ombo).", "provinsi": "Jateng", "efishery": 0}, {"x0": 1.380979434987793, "x1": -0.7036070535092684, "text": "Potensi cukup besar, sebagai barometer budidaya udang nasional. Fase 1 karena dengan skema sewa akan menarik minat petambak untuk coba efisehry", "provinsi": "Jatim", "efishery": 1}, {"x0": 2.3058753588287457, "x1": -0.20880452804474622, "text": "Kemaren sempat visit dengan sales pakan shinta , di Lamongan potensi kolam lumayan  banyak,ada salah satu agen&petani berminat untuk AF eFishery akan tetapi area kolam yg mungkin jauh dengan instalasi listrik ,", "provinsi": "Jatim", "efishery": 0}, {"x0": 5.775971594415034, "x1": 1.1348868947404338, "text": "wilayah ini merupakan wilayah produsen nila terbesar di Lampung. beberapa owner disana sudah sounding berminat ingin mencoba AF efishery. informasi terakhir yang didapat, produksi ikan di Ranau belum mampu memenuhi total demand ikan nila ranau di pasar. sehingga teknologi autofeeder kita diharapkan mampu meningkatkan produksi nila ranau. disamping itu, kendala permodalan petani juga menjadi salah satu kendala penting", "provinsi": "Lampung", "efishery": 50}, {"x0": 6.975409168317715, "x1": 0.9172492838826841, "text": "wilayah ini merupakan wilayah produsen lele di Lampung. wilayah ini memiliki potensi yang besar untuk market feeder dan pakan. kendala petani di daerah ini, sama seperti wilayah lain, keterbatasan modal dan market. market yang ada saat ini umumnya ikan lele dijual ke market lokal (Lampung) dan Sumatera Selatan.", "provinsi": "Lampung", "efishery": 0}, {"x0": 4.817756877685264, "x1": -0.2733277370961598, "text": "wilayah ini merupakan salah satu wilayah produsen patin dan lele terbesar se Sumatera Selatan. potensi penjualan feeder di daerah ini sangat besar, namun keterbatasan petani akan modal dan pasar menjadi kendala utama. sehingga banyak petani yang tidak mau tebar dengan kepadatan tinggi. akibatnya, secara analisa usahatani dirasa berat jika ditambah beban cost sewa autofeeder", "provinsi": "Sumsel", "efishery": 0}, {"x0": 4.440263757524648, "x1": -0.13856059087513398, "text": "di area ini sudah ada feeder yang ter deploy. kita sudah ber mitra dengan agen pakan. tinggal dibuat konsep untuk bundling. sama seperti di Palas, namun mitra kita ini hanya toko bukan petani.", "provinsi": "Lampung", "efishery": 4}]}}, {"mode": "vega-lite"});
</script>




```python
# cek terlebih dahulu summary kandidat area per provinsi
def areaExploration(df_area):
    print("Deskripsi statistik:")
    print(df_area.describe())
    print("\n")
    
    # lihat top data
    print("Top rank data:")
    df_area.sort_values(inplace=True, ascending=False)
    print(df_area)
    print("\n")
    
    # histogram
    print("Sebaran datanya")
    n_bins = int(np.sqrt(df_area.count()))
    df_area.plot(kind='hist', bins=n_bins, figsize=(12,6), facecolor='grey', edgecolor='black')
    
```

```python
print("=== EKSPLORASI KANDIDAT AREA PER PROVINSI ===")
df_city_kand = df_canvassing_clean.groupby(['provinsi'])['kandidat_area_id'].nunique()
areaExploration(df_city_kand)
```
    === EKSPLORASI KANDIDAT AREA PER PROVINSI ===
    Deskripsi statistik:
    count    15.000000
    mean      3.133333
    std       3.090693
    min       1.000000
    25%       1.000000
    50%       2.000000
    75%       4.000000
    max      10.000000
    Name: kandidat_area_id, dtype: float64
    
    
    Top rank data:
    provinsi
    Jatim       10
    Jabar       10
    Jateng       5
    NTB          4
    Kalsel       4
    Lampung      3
    Sumsel       2
    Bengkulu     2
    Sumbar       1
    Sulut        1
    Riau         1
    Kalteng      1
    Jambi        1
    DIY          1
    Bali         1
    Name: kandidat_area_id, dtype: int64
    
    
    Sebaran datanya




![png](images/output_41_1.png)


Disini kita bisa tahu kalau rata-rata per provinsi akan ada 3 kandidat area baru. Kemudian dari Top Rank diatas, **Jatim** dan **Jabar** menjadi daerah utama dalam ekspansi efishery. Mari kita cari tahu alasannya.


```python
df_kand_area_kolam = pd.concat([df_agg_area, df_agg_petani, df_agg_kolam], axis=1)
df_kand_area_kolam.sort_values(by='kandidat_area_id')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>kandidat_area_id</th>
      <th>jumlah_petani_efishery</th>
      <th>total_jumlah_petani</th>
      <th>jumlah_kolam</th>
    </tr>
    <tr>
      <th>provinsi</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Bali</th>
      <td>1</td>
      <td>0</td>
      <td>50</td>
      <td>100</td>
    </tr>
    <tr>
      <th>DIY</th>
      <td>1</td>
      <td>0</td>
      <td>200</td>
      <td>500</td>
    </tr>
    <tr>
      <th>Jambi</th>
      <td>1</td>
      <td>0</td>
      <td>200</td>
      <td>5000</td>
    </tr>
    <tr>
      <th>Kalteng</th>
      <td>1</td>
      <td>2</td>
      <td>50</td>
      <td>150</td>
    </tr>
    <tr>
      <th>Riau</th>
      <td>1</td>
      <td>10</td>
      <td>200</td>
      <td>5000</td>
    </tr>
    <tr>
      <th>Sulut</th>
      <td>1</td>
      <td>0</td>
      <td>300</td>
      <td>1000</td>
    </tr>
    <tr>
      <th>Bengkulu</th>
      <td>2</td>
      <td>55</td>
      <td>200</td>
      <td>1200</td>
    </tr>
    <tr>
      <th>Sumbar</th>
      <td>2</td>
      <td>17</td>
      <td>500</td>
      <td>2000</td>
    </tr>
    <tr>
      <th>Sumsel</th>
      <td>2</td>
      <td>30</td>
      <td>250</td>
      <td>1200</td>
    </tr>
    <tr>
      <th>Lampung</th>
      <td>3</td>
      <td>54</td>
      <td>300</td>
      <td>500</td>
    </tr>
    <tr>
      <th>Kalsel</th>
      <td>4</td>
      <td>45</td>
      <td>659</td>
      <td>1000</td>
    </tr>
    <tr>
      <th>NTB</th>
      <td>4</td>
      <td>7</td>
      <td>110</td>
      <td>440</td>
    </tr>
    <tr>
      <th>Jateng</th>
      <td>5</td>
      <td>10</td>
      <td>850</td>
      <td>3900</td>
    </tr>
    <tr>
      <th>Jatim</th>
      <td>10</td>
      <td>15</td>
      <td>1500</td>
      <td>7900</td>
    </tr>
    <tr>
      <th>Jabar</th>
      <td>12</td>
      <td>9</td>
      <td>5500</td>
      <td>14520</td>
    </tr>
  </tbody>
</table>
</div>



Disini kita bisa tahu alasan Jatim dan Jabar jadi tujuan ekpansi efishery karena jumlah kolam dan petaninya yg paling banyak. Sehingga potensi perikanannya juga tinggi. Untuk selanjutnya, pihak efishery bisa meningkatkan perhatiannya pada daerah2 yang potensi perikannya tinggi, seperti di **Jabar** dan **Jatim**.


```python
df_agg_city.columns
```




    Index(['jumlah_petani_efishery', 'total_jumlah_petani', 'jumlah_kolam',
           'produksi_ikan', 'penjualan_ikan', 'density_petani_efishery',
           'density_kolam_petani', 'density_produksi_ikan', 'density_penjualan'],
          dtype='object')



### Summary Data


```python
total_data = df_canvassing_clean['provinsi_id'].nunique()
total_petani_efishery = df_agg_city['jumlah_petani_efishery'].sum()
total_petani = df_agg_city['total_jumlah_petani'].sum()
total_kolam = df_agg_city['jumlah_kolam'].sum()
total_produksi = df_agg_city['produksi_ikan'].sum()
total_penjualan = df_agg_city['penjualan_ikan'].sum()
total_kandidat_area = df_canvassing_clean['kandidat_area_id'].sum()

print("Total data yang tercatat: {} Provinsi".format(total_data))
print("Total petani efishery yang tercatat: {} orang".format(total_petani_efishery))
print("Total petani yang tercatat: {} orang".format(total_petani))
print("Total kolam yang tercatat: {} kolam".format(total_kolam))
print("Total produksi yang tercatat: {} ton".format(total_produksi))
print("Total penjualan yang tercatat: {} ton".format(total_penjualan))
print("Total kandidat area yang tercatat: {} Kabupaten".format(total_kandidat_area))
```
    Total data yang tercatat: 15 Provinsi
    Total petani efishery yang tercatat: 254 orang
    Total petani yang tercatat: 10869 orang
    Total kolam yang tercatat: 44410 kolam
    Total produksi yang tercatat: 86195 ton
    Total penjualan yang tercatat: 41595 ton
    Total kandidat area yang tercatat: 1153 Kabupaten


### Top Area Petani


```python
df_prov['total_jumlah_petani'].sort_values().plot.barh();
```


![png](images/output_49_0.png)


Dari sini, kita bisa tahu kalau **Rank Top 5**, dari jumlah petani adalah:
1. **Jabar**
2. **Jatim**
3. **Jateng**
4. **Kalsel**
5. **Sumbar**

### Top Area Petani efihsery


```python
df_prov['jumlah_petani_efishery'].sort_values().plot.barh();
```


![png](images/output_52_0.png)


Dari sini kita bisa tahu kalau **Rank Top 5** jumlah petani yg sudah jadi client eFishery adalah:
1. **Bengkulu**
2. **Lampung**
3. **Kalsel**
4. **Sumsel**
5. **Sumbar**

Hmm, benar2 berbeda dengan **Rank Top 5** total **jumlah petani total** per provinsinya,
1. **Jabar**
2. **Jatim**
3. **Jateng**
4. **Kalsel**
5. **Sumbar**

Jika kita lihat dari density petani efisherynya, Rank Top 5 jumlah petani efishery relatif lebih besar dibandingkan dengan density petani eFishery di Rank Top 5 Jumlah petani total. Jadi, disini eFishery perlu memberikan perhatian lebih ke  kelima daerah tersebut sangat besar, karena dengan alasan:
- **Lebih dekat dengan kantor pusat**
- **jumlah petani banyak**
- **Kerapatan petani efisherynya masih sangat renggang**
- **jumlah kolamnya juga banyak**

### Top Area Jumlah Kolam


```python
print(df_agg_city.sort_values('jumlah_kolam', ascending = False).head(5))
```
              jumlah_petani_efishery  total_jumlah_petani  jumlah_kolam  \
    provinsi                                                              
    Jabar                          9                 5500         14520   
    Jatim                         15                 1500          7900   
    Jambi                          0                  200          5000   
    Riau                          10                  200          5000   
    Jateng                        10                  850          3900   
    
              produksi_ikan  penjualan_ikan  density_petani_efishery  \
    provinsi                                                           
    Jabar             14845           20710                 0.001636   
    Jatim              4170            6800                 0.010000   
    Jambi              1000            1500                 0.000000   
    Riau              50000            3000                 0.050000   
    Jateng             1650            2050                 0.011765   
    
              density_kolam_petani  density_produksi_ikan  density_penjualan  
    provinsi                                                                  
    Jabar                 2.640000               1.022383           1.426309  
    Jatim                 5.266667               0.527848           0.860759  
    Jambi                25.000000               0.200000           0.300000  
    Riau                 25.000000              10.000000           0.600000  
    Jateng                4.588235               0.423077           0.525641  



```python
df_canvassing_clean.columns
```




    Index(['kandidat_area', 'provinsi', 'fase_peluncuran',
           'jumlah_petani_efishery', 'total_jumlah_petani', 'jumlah_kolam',
           'produksi_ikan', 'penjualan_ikan', 'komoditas_utama',
           'market_leader_pakan', 'provinsi_id', 'fase_peluncuran_id',
           'kandidat_area_id'],
          dtype='object')




```python
#pivot_fase = pd.pivot_table(df_canvassing_clean.drop(['provinsi_id','kandidat_area_id','fase_peluncuran_id'], axis=1), index=['provinsi','kandidat_area'])
#pivot_fase.sort_values('jumlah_petani_efishery', ascending=False)
```

```python
df_agg_city.columns
```




    Index(['jumlah_petani_efishery', 'total_jumlah_petani', 'jumlah_kolam',
           'produksi_ikan', 'penjualan_ikan', 'density_petani_efishery',
           'density_kolam_petani', 'density_produksi_ikan', 'density_penjualan'],
          dtype='object')




```python
#pivot_fase_area = pd.pivot_table(df_agg_city.drop(['jumlah_kolam','produksi_ikan','penjualan_ikan','jumlah_petani_efishery','total_jumlah_petani',
#                                                  'density_kolam_petani','density_petani_efishery'], axis=1), index=['provinsi'])
#pivot_fase_area
```

```python
#pivot_fase_area.plot(kind='bar', figsize=(10,5))
```

```python
#import networkx as nx
#from tqdm import tqdm

# construct graph
#G = nx.from_pandas_edgelist(df_canvassing_clean, 'market_leader_pakan', 'komoditas_utama', ['provinsi'], create_using=nx.MultiDiGraph())

# get edge weights
#_, wt = zip(*nx.get_edge_attributes(G, 'provinsi').items())
```

```python
# plot graph
#plt.figure(figsize=(15,15))
#pos = nx.spring_layout(G, k = 500, seed = 21) # k regulates the distance between nodes
#nx.draw(G, with_labels=True, node_color='skyblue', node_size=4000, pos = pos, edgelist=G.edges(), edge_color='g', arrowsize=15)
#plt.show()
```
### Kesimpulan

Dari hasil eksplorasi data diatas, ada beberapa kesimpulan yang dapat diutarakan:

Data survey yang kita miliki meliputi:
- **15 Provinsi**
- **47 Kandidat Area**

Hal menarik yg ditemukan dalam data ini adalah:
- **>50% memiliki kesamaan dalam market leader**
- **Hampir 50% setiap area memiliki komoditas ikan utama yang sama**

Mayoritas, petani efishery berada di **fase F2-F3** (64%), kemudian disusul oleh F4 (20%), dan F1 (16%).

**Kepadatan** petani efishery masih sangat kecil, terutama didaerah Rank Top 5 jumlah petani total, yg kepadatannya dibawah 1%.

**Penjualan ikan** oleh para petani cenderung masih belum efisien, karena masih ada yang produksinya besar, tapi penjualannya rendah (mungkin efishery fresh bisa jadi solusi)

**Jatim** dan **Jabar** memiliki potensi perikanan yang tinggi, namun petani efisherynya masih sedikit, jadi untuk kedepannya bisa diberikan perhatian lebih lagi ke kedua daerah tersebut.

Dari data yang ada ini, dapat diketahui juga kalau:
- Total data yang tercatat: 15 Provinsi
- Total petani efishery yang tercatat: 254 orang
- Total petani yang tercatat: 10869 orang
- Total kolam yang tercatat: 44410 kolam
- Total produksi yang tercatat: 86195 ton
- Total penjualan yang tercatat: 41595 ton
- Total kandidat area yang tercatat: 1153 Kabupaten

Hanya **Kalsel** yang masuk ke Rank Top 5 jumlah petani efishery dan jumlah petani total, dengan Rank Top 5 jumlah petani efishery berturut-turut: Bengkulu-Lampung-**Kalsel**-Sumsel-Sumbar. Kemudian Rank Top 5 jumlah petani total berturut-turut: Jabar-Jatim-Jateng-**Kalsel**-Sumbar. Sehingga efishery perlu memberikan perhatian lebih ke daerah seperti Jabar, Jatim, dan Jateng, karena menimbang:
- **Lebih dekat dengan kantor pusat**
- **jumlah petani banyak**
- **Kerapatan petani efisherynya masih sangat renggang (> 1%)**
- **jumlah kolamnya juga banyak**

Adapun Rank Top 5 **Jumlah Kolam** berturut-turut: Jabar - Jatim - Jambi - Riau - Jateng.


### Perbandingan dengan data bi-dash Canvassing Sales Area
Kalau dilihat dari Rank Top 5 Jumlah Petani, ada kesesuaian antara kedua data ini, dengan Rank Top 5 petani data Canvassing Sales berturut-turut: Subang (**Jabar**) - Kampar (Jambi) - Tulungagung (**Jatim**) - Kediri (**Jatim**) - Tasik (**Jabar**). Sedangkan untuk data ini, Rank Top 5 Jumlah petani total berturut-turut: **Jabar** - **Jatim** - Jateng - Kalsel - Sumbar.

Kemudian, kalau dilihat dari Rank Top 5 Jumlah kolam, ditemukan adanya kesesuaian antara kedua data ini, dengan Rank Top 5 kolam data Canvassing Sales berturut-turut: Tulungagung (**Jatim**) - Kediri (**Jatim**) - Muaro Jambi (**Jambi**) - Subang (**Jabar**) - Kampar (**Riau**). Seangkan untuk data ini, Rank Top 5 Jumlah Kolam berturut-turut: **Jabar** - **Jatim** - **Jambi** - **Riau** - Jateng.
### Saran dan rekomendasi
Kalau dilihat dari datanya, masih ada data yang berupa **estimasi**, jadi validitas datanya belum bisa dipercaya 100%. Selain itu, disini juga belum melakukan uji statistik, sehingga secara statistikpun, datanya tidak bisa jadi acuan.

Jumlah data masih rendah, sehingga tidak banyak yang bisa dilakukan pada data ini. Untuk kedepannya, diharapkan bisa mengumpulkan data lebih banyak lagi. Selain itu, disini juga ada **data teks**, dalam hal ini belum diproses dengan baik, sehingga insightnya dari data tersebutpun belum diesktraks. Untuk selanjutnya, perlu ada pemrosesan data teks, supaya insight yang didapat bisa lebih banyak.

### Referensi
- Untuk Split data dalam kolom: [referensi 1](https://kite.com/python/answers/how-to-split-a-pandas-dataframe-column-in-python), [referensi 2](https://www.geeksforgeeks.org/python-pandas-split-strings-into-two-list-columns-using-str-split/)
- https://www.analyticsvidhya.com/blog/2020/02/network-analysis-ipl-data/


```python

```