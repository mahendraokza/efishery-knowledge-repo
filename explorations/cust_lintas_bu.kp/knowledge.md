---
title: Eksplorasi Data Cust Lintas BU
authors:
- Nikmatun Aliyah Salsabila
tags:
- eksplorasi
- business unit
created_at: 2016-06-25 00:00:00
updated_at: 2020-07-06 18:03:24.072674
tldr: This is short description of the content and findings of the post.
thumbnail: images/output_14_0.png
---

## Permasalahan


### Latar Belakang Masalah
Sebelum bisa melakukan segmentasi customer dan eksperimen lainnya, perlu dipersiapkan datanya terlebih dahulu. Tim data eFishery sudah mencoba membuat data raw customer lintas business unit.

### Dugaan Masalah
> Apakah kualitas data kita sudah baik? 

### Batasan Masalah


### Kamus Istilah
- Customers = Petani = Farmers
- Records = Baris = Instances

Jika ada perbedaan penulisan istilah pada eksplorasi ini, mohon dimaafkan.


## Pengumpulan Data

### Sumber Datanya
[Raw Cust Lintas BU](https://bi-dash.efishery.com/question/3770)  

### Pengertian Sumber Data



```python
from google.colab import drive
drive.mount('/content/drive')
```
    Drive already mounted at /content/drive; to attempt to forcibly remount, call drive.mount("/content/drive", force_remount=True).


## Eksplorasi Data


```python
from IPython.display import display, Math, HTML
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import random
random.seed(100)

# from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.decomposition import TruncatedSVD
# from sklearn.manifold import TSNE
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

RANDOM_STATE = 101

%matplotlib inline
```

```python
sns.set_context('talk')
plt.rcParams['figure.figsize'] = (12,7)
```

```python
data = pd.read_csv('https://bi-dash.efishery.com/public/question/d7a06d45-87e2-4467-a71f-b87d589c15d0.csv')
```

```python
# ubah ktp biar jadi string aja
data['ID Number'] = data['ID Number'].fillna(0)
data['ID Number'] = data['ID Number'].astype('int').astype('str')
data['ID Number'] = data['ID Number'].replace('0', np.nan)

# udah dicek hasilnya sama kayak isnull di data aslinya
```
### Kualitas data: Gambaran isi data per kolom


Sebelum cek kelengkapan datanya, mari lihat gambaran umum data!


```python
print('Total records: {}'.format(data.shape[0]))
```
    Total records: 1545


#### Province


```python
# print('Total Provinsi: {}'.format(data['Province'].nunique()))
display(HTML("""Terdapat {:,} provinsi. Provinsi ini isinya termasuk 
    negara lain (India, Bangladesh, Thailand, Vietnam). 
    Selain itu, ada {:,} records yang kosong pada kolom Province.""".format(
    data['Province'].nunique(),
    data['Province'].isnull().sum()
)))
```


Terdapat 30 provinsi. Provinsi ini isinya termasuk 
    negara lain (India, Bangladesh, Thailand, Vietnam). 
    Selain itu, ada 151 records yang kosong pada kolom Province.



```python
ax = sns.barplot(
    y=data['Province'].value_counts().head(15).index,
    x=data['Province'].value_counts().head(15),
    color='royalblue'
)

for p in ax.patches:
    ax.annotate("%.0f" % p.get_width(), 
                xy=(p.get_width(), 
                    p.get_y()+p.get_height()/2
                    ),
                xytext=(5, 0), textcoords='offset points', 
                ha="left", va="center", fontsize=10
                )
    
plt.title('Top 15 Provinces')
plt.xlabel('')
plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_14_0.png)


#### Region


```python
display(HTML("""Ada {:,} region di Indonesia dalam data ini. Tidak ada 
    value region untuk negara lain. {:,} records kosong pada kolom Region.
    """.format(
    data['Region'].nunique(),
    data['Region'].isnull().sum()
)))
```


Ada 158 region di Indonesia dalam data ini. Tidak ada 
    value region untuk negara lain. 189 records kosong pada kolom Region.




```python
ax = sns.barplot(
    y=data['Region'].value_counts().head(15).index,
    x=data['Region'].value_counts().head(15),
    color='royalblue'
)
for p in ax.patches:
    ax.annotate("%.0f" % p.get_width(), 
                xy=(p.get_width(), 
                    p.get_y()+p.get_height()/2
                    ),
                xytext=(5, 0), textcoords='offset points', 
                ha="left", va="center", fontsize=10
                )
    
plt.title('Top 15 Regions')
plt.xlabel('')
plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_17_0.png)


#### Komoditas


```python
display(HTML("""Total records yang tidak ada komoditasnya sebanyak {:,}.
    Terdapat {:,} kombinasi komoditas, tetapi penulisannya tidak seragam.
    Oleh karena itu, penulis akan memprosesnya terlebih dahulu.
    Isi kolom komoditas pada data ini sebelum distandarkan adalah sebagai berikut:.
    """.format(
        data['Komoditas'].isnull().sum(),
        data['Komoditas'].nunique()
    # data[data['Komoditas'].notnull()]['Komoditas'].unique()
)))
```


Total records yang tidak ada komoditasnya sebanyak 825.
    Terdapat 50 kombinasi komoditas, tetapi penulisannya tidak seragam.
    Oleh karena itu, penulis akan memprosesnya terlebih dahulu.
    Isi kolom komoditas pada data ini sebelum distandarkan adalah sebagai berikut:.




```python
data['Komoditas'].unique()
```




    array(['Lele', 'Udang Vanname', 'Nila', nan, 'Mas', 'Gurame', 'Udang',
           'Ikan Patin', 'Patin', 'Ikan Lele dan Patin', 'Gabus', 'Bandeng',
           'Ikan Patin  dan Gurame', 'Ikan Nila dan Mas',
           'Ikan Nila, Patin dan Mas', 'Ikan Patin dan Gurami', 'Ikan Lele',
           'Ikan Lele ', 'Ikan Nila', 'Ikan Nila dan Lele',
           'Ikan Nila dan Gurame', 'Ikan Lele dan Nila',
           'Ikan Lele dan Gurame', 'Ikan Bandeng dan Udang',
           'Ikan Mas dan Nila', 'Nila/ Mas ', 'Ikan Nila, Mas dan Gurame',
           'Ikan Lele dan Gurami', 'Ikan Patin dan Gabus',
           'Ikan Lele, Nila, Udang, Bandeng', 'Ikan Nila, Mas dan Nilem',
           'Ikan NIla, Mas dan Gurame', 'Bawal', 'Ikan Mas',
           'Ikan Lele, Patin dan Gurame', 'Ikan Nila  dan Mas',
           'Ikan Patin dan Lele', 'Ikan Gurame',
           'Ikan Lele, Nila, Patin dan Gurame', 'Ikan Mujaer & Nila',
           'Ikan Nila dan Bawal', 'Ikan Nila, Udang, Bandeng',
           'Ikan Nila, Mas dan Bawal', 'Kakap', 'Ikan Lele, Mas dan Patin',
           'Ikan Lele, Nila, Mas dan Gurame', 'Ikan Gurami', 'Nila/Mas',
           'Ikan Mas ', 'Ikan Pati dan Gurame', ' Gurame / Lele'],
          dtype=object)




```python
# lowercase, replace beberapa string
data['Komoditas'] = data['Komoditas'].str.lower().str.lstrip().str.rstrip()
data['Komoditas'] = data['Komoditas'].str.replace('ikan','')
pattern = '|'.join([' dan ','&','/', ' / ', ' & ', ', ', ' ,'])
data['Komoditas'] = data['Komoditas'].str.replace(pattern, ',')
data['Komoditas'] = data['Komoditas'].str.replace('udang vanname', 'udang_vanname') # biar pas bikin bow nggak misah
data['Komoditas'] = data['Komoditas'].str.lstrip().str.rstrip()
```

```python
# buat bag of words karena ada permasalahan urutan penulisan, jadi nanti biar seragam semua.
from sklearn.feature_extraction.text import CountVectorizer
vect = CountVectorizer()
X = vect.fit_transform(data['Komoditas'].values.astype('U'))

feature_names = vect.get_feature_names()
bow_komoditas = pd.DataFrame(X.toarray(), columns=feature_names)
```

```python
data = data.merge(bow_komoditas, left_index=True, right_index=True)
```

```python
# mengubah isi kolom komoditas

k = data.iloc[:,12:26]

data['Komoditas'] = k[(k != 0).any(1)].dot(k.columns+',').str.rstrip(',')
data['Komoditas'] = data['Komoditas'].str.lstrip().str.rstrip()

#replace string nan dengan NaN
data['Komoditas'] = data['Komoditas'].replace('nan', np.nan)
```

```python
kmdts = data[['bandeng', 'bawal', 'gabus', 'gurame', 'kakap', 
              'lele', 'mas', 'mujaer','nila', 'nilem', 'patin', 
              'udang', 'udang_vanname']].sum().sort_values(ascending=False)

ax = sns.barplot(
    y=kmdts.index,
    x=kmdts.values,
    color='royalblue'
)
for p in ax.patches:
    ax.annotate("%.0f" % p.get_width(), 
                xy=(p.get_width(), 
                    p.get_y()+p.get_height()/2
                    ),
                xytext=(5, 0), textcoords='offset points', 
                ha="left", va="center", fontsize=10
                )
    
plt.title('Jumlah masing-masing komoditas (total termasuk kombinasinya)')
plt.xlabel('# customers')
plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_25_0.png)



```python
data['Jumlah Komoditas'] = data['Komoditas'].str.split(',').str.len()
```

```python
display(HTML("""Pada data ini customers ada yang memiliki
    {:,} hingga {:,} komoditas, dan kebanyakan (50% di antaranya) 
    memiliki {:,} komoditas. Setelah diproses, total kombinasi 
    komoditas sebanyak {:,}.""".format(
    data['Jumlah Komoditas'].min(),
    data['Jumlah Komoditas'].max(),
    data['Jumlah Komoditas'].median(),
    data['Komoditas'].nunique()
    )))

```


Pada data ini customers ada yang memiliki
    1.0 hingga 4.0 komoditas, dan kebanyakan (50% di antaranya) 
    memiliki 1.0 komoditas. Setelah diproses, total kombinasi 
    komoditas sebanyak 32.



```python
print('Jumlah per kombinasi komoditas:')
data['Komoditas'].value_counts()
```
    Jumlah per kombinasi komoditas:






    lele                      143
    nila                      123
    patin                     120
    mas                        65
    mas,nila                   49
    gurame                     21
    lele,patin                 15
    lele,nila                   6
    gurame,lele                 4
    mas,nila,patin              3
    gurame,nila                 3
    bandeng                     2
    gurami,patin                2
    gurame,mas,nila             2
    gurami,lele                 2
    mas,nila,nilem              2
    kakap                       1
    gurame,pati                 1
    mujaer,nila                 1
    gurami                      1
    gabus,patin                 1
    bawal,mas,nila              1
    gurame,lele,patin           1
    bandeng,lele,nila           1
    bawal                       1
    gurame,lele,nila,patin      1
    gabus                       1
    gurame,lele,mas,nila        1
    bawal,nila                  1
    gurame,patin                1
    lele,mas,patin              1
    bandeng,nila                1
    Name: Komoditas, dtype: int64




Kesimpulan dan Rekomendasi:
- Jika memungkinkan untuk diubah, sepertinya input untuk komoditas ini jangan dibuat free text karena:
  - Penulisannya tidak seragam. Meskipun dibuatkan standarnya, tetapi kemungkinan terjadi kesalahan masih ada.
  - Rentan salah ketik
  - Masalah perbedaan urutan pada string; 'Nila, Mas' akan terhitung beda dengan 'Mas, Nila' ketika diolah

- Komoditasnya menjadi kolom boolean sejak awal terlihat lebih baik. Diprosesnya lebih mudah.

- Kalau tidak mungkin diubah untuk pencatatan awal, berarti harus *dibersihkan* dulu saat query datanya.

### Business Unit


```python
# ## membuat kolom business unit biar memudahkan
# d = data.iloc[:,1:5]
# data['BU'] = d.notna().dot(d.columns+',').str.rstrip(',')
# data['BU'] = data['BU'].str.replace('Cust,?' , '')
# data['BU'] = data['BU'].str.lstrip()

# ## buat dataframe baru
# df = data.iloc[:,5:]
```

```python
# membuat kolom business unit biar memudahkan
d = data.iloc[:,1:4]
data['BU'] = d.notna().dot(d.columns+',').str.rstrip(',')
data['BU'] = data['BU'].str.replace('Cust,?' , '')
data['BU'] = data['BU'].str.lstrip()

data['BU'] = data['BU'].replace('', 'Registered members, no BU')

# buat dataframe baru
df = data.copy()
```

```python
# ini biar yang angkanya > 1 (tanda duplikat) jadi 1 aja, sih .-. biar 1 sama null aja ee
a = max([df['Cust Fresh'].max(), df['Cust Feeder'].max(), 
         df['Cust Feedfund'].max(), df['Cust Regmem'].max()])
m = list(range(2,int(a)))

df['Cust Regmem'] = df['Cust Regmem'].replace(m, 1).fillna(0)
df['Cust Feedfund'] = df['Cust Feedfund'].replace(m, 1).fillna(0)
df['Cust Feeder'] = df['Cust Feeder'].replace(m, 1).fillna(0)
df['Cust Fresh'] = df['Cust Fresh'].replace(m, 1).fillna(0)
```

```python
cust = df[['Cust Feeder', 'Cust Feedfund', 
              'Cust Fresh', 'Cust Regmem']]

cust = cust.rename(
    columns={
        'Cust Feeder': 'Feeder', 
        'Cust Feedfund': 'Feedfund', 
        'Cust Fresh': 'Fresh', 
        'Cust Regmem': 'Registered Members'
        }
    ).sum().sort_values(ascending=False)

ax = sns.barplot(
    y=cust.index,
    x=cust.values,
    color='royalblue'
)
for p in ax.patches:
    ax.annotate("%.0f" % p.get_width(), 
                xy=(p.get_width(), 
                    p.get_y()+p.get_height()/2
                    ),
                xytext=(5, 0), textcoords='offset points', 
                ha="left", va="center", fontsize=10
                )
    
plt.title('Jumlah masing-masing business unit & registered members')
plt.xlabel('# customers')
plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_34_0.png)



```python
ax = sns.barplot(
    y=df['BU'].value_counts().index,
    x=df['BU'].value_counts(),
    color='royalblue'
)

for p in ax.patches:
    ax.annotate("%.0f" % p.get_width(), 
                xy=(p.get_width(), 
                    p.get_y()+p.get_height()/2
                    ),
                xytext=(5, 0), textcoords='offset points', 
                ha="left", va="center", fontsize=10
                )
    
plt.title('Irisan customers lintas business unit')
plt.xlabel('')
plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_35_0.png)



```python
display(HTML("""Total registered members sebanyak {:,} customers ({:,}%), 
    sementara yang tidak terdaftar ada {:,} customers ({:,}%).
    """.format(
        data[df['Cust Regmem'] > 0]['Cust Regmem'].count(),
        round((df[df['Cust Regmem'] == 1]['Cust Regmem'].count() / df.shape[0]) * 100, 2),
        df[df['Cust Regmem'] == 0]['Cust Regmem'].count(),
        round((df[df['Cust Regmem'] == 0]['Cust Regmem'].count() / df.shape[0]) * 100, 2)
    # data[data['Komoditas'].notnull()]['Komoditas'].unique()
)))
```


Total registered members sebanyak 659 customers (42.65%), 
    sementara yang tidak terdaftar ada 886 customers (57.35%).




```python
# membership_bu = df.groupby(
#     ['Cust Regmem', 'BU']
#     )['Row Number'].count().reset_index()

# membership_bu = membership_bu.replace([1,0],['Member', 'Non-Member'])
# membership_bu = membership_bu.rename(columns={
#     'Cust Regmem': 'Membership'
# })
```

```python
temp = df[['Cust Regmem', 'Cust Fresh', 
           'Cust Feeder', 'Cust Feedfund']].melt(
               id_vars=["Cust Regmem"], 
                var_name="BU", 
                value_name="Value")

membership_bu2 = temp.groupby(
    ['Cust Regmem', 'BU']
    )['Value'].sum().reset_index()

membership_bu2 = membership_bu2.replace([1,0],['Member', 'Non-Member'])
membership_bu2 = membership_bu2.rename(columns={
    'Cust Regmem': 'Membership'
})
membership_bu2['BU'] = membership_bu2['BU'].str.replace('Cust', '')
```

```python
ax = sns.barplot(
    x = 'Value', 
    y = 'BU',
    hue = 'Membership',
    data = membership_bu2[membership_bu2['BU'] != 'Registered members, no BU'],
    # color = 'royalblue'
)

for p in ax.patches:
    ax.annotate("%.0f" % p.get_width(), 
                xy=(p.get_width(), 
                    p.get_y()+p.get_height()/2
                    ),
                xytext=(5, 0), textcoords='offset points', 
                ha="left", va="center", fontsize=10
                )
    
plt.title('Keanggotaan masing-masing BU')
plt.xlabel('')
plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_39_0.png)


#### Phone

Beberapa nomor diragukan kevalidannya. Di Indonesia sepertinya tidak ada kode area dengan diawali 01xxx (jika 62 sudah pasti diterjemahkan menjadi 0). Nomor HP pun sepertinya tidak ada yang begitu.


```python
data[data['Phone'].astype('str').str.startswith('621')].iloc[:,2:12]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Cust Feedfund</th>
      <th>Cust Fresh</th>
      <th>Cust Regmem</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Nama</th>
      <th>Phone</th>
      <th>Province</th>
      <th>Region</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>lele</td>
      <td>511najkdcbv9</td>
      <td>MOCHAMAD ALI BADRUS</td>
      <td>62123</td>
      <td>JAWA TIMUR</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>26025973311001</td>
      <td>NaN</td>
      <td>511f3ve4a6fr</td>
      <td>PT. Dua Putra Perkasa</td>
      <td>62129555555</td>
      <td>BENGKULU</td>
      <td>KABUPATEN KAUR</td>
    </tr>
    <tr>
      <th>2</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3201074309900001</td>
      <td>nila</td>
      <td>512992axxju8</td>
      <td>Rohaidi Jafar</td>
      <td>62137741289</td>
      <td>JAWA BARAT</td>
      <td>KABUPATEN BANDUNG</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1605200506810003</td>
      <td>nila</td>
      <td>511fjfvg2kp6</td>
      <td>Beni Afriansah</td>
      <td>621377529022</td>
      <td>SUMATERA SELATAN</td>
      <td>KABUPATEN MUSI RAWAS</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Abdi Rahayu</td>
      <td>621936788505</td>
      <td>NUSA TENGGARA BARAT</td>
      <td>KABUPATEN LOMBOK BARAT</td>
    </tr>
  </tbody>
</table>
</div>




```python
phone_duplicate = data[data['Phone'].isin(
    data['Phone'].value_counts(
    )[data['Phone'].value_counts() > 1].index.tolist()
    )
]
```

```python
display(HTML("""Ada {:,} nomor kontak yang memiliki baris > 1.
    Kasus ini terdapat pada data dengan region {}""".format(
    phone_duplicate['Phone'].nunique(),
    phone_duplicate['Region'].unique()
    )))

```


Ada 6 nomor kontak yang memiliki baris > 1.
    Kasus ini terdapat pada data dengan region ['TENJOLAYA']



```python
phone_duplicate.iloc[:,1:12]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Cust Feeder</th>
      <th>Cust Feedfund</th>
      <th>Cust Fresh</th>
      <th>Cust Regmem</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Nama</th>
      <th>Phone</th>
      <th>Province</th>
      <th>Region</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>559</th>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3201170106790005</td>
      <td>mas,nila</td>
      <td>16fcb2d6-721b-12ee-b287-5164045a5668</td>
      <td>SYAHRIAN</td>
      <td>6281585901341</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>560</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>3201171004490001</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Ripai</td>
      <td>6281585901341</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1274</th>
      <td>1.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>623266653434000</td>
      <td>mas,nila</td>
      <td>511fjm8h3n4f</td>
      <td>Anshori</td>
      <td>6285693333594</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1275</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>3201171103570003</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Anshori</td>
      <td>6285693333594</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1299</th>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3201172304840006</td>
      <td>mas,nila</td>
      <td>16fcb128-8949-9fe6-408d-0a9eed57344f</td>
      <td>ANDI IMAM MAULANA</td>
      <td>6285717000297</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1300</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>3201172304840006</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Andi Imam Maulana</td>
      <td>6285717000297</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1305</th>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3201171505760009</td>
      <td>mas,nila</td>
      <td>170a4773-ec41-bbd3-a16a-d420eeff97f8</td>
      <td>UJANG HOLIL</td>
      <td>6285719669587</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1306</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>3201175505760010</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Ujang Holil</td>
      <td>6285719669587</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1351</th>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
      <td>3201171506590015</td>
      <td>mas</td>
      <td>16fcb169-5c96-154d-25fd-937b7e35331b</td>
      <td>Surya Mad Uhi</td>
      <td>6285779380756</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1352</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>3201171506590015</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Baban Subandi</td>
      <td>6285779380756</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1354</th>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3201170107770004</td>
      <td>mas,nila</td>
      <td>170a49fc-813f-6b51-6b3c-5d4e5d81964d</td>
      <td>SOLEHUDIN</td>
      <td>6285780459878</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
    <tr>
      <th>1355</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>3201175601810000</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>Solehudin</td>
      <td>6285780459878</td>
      <td>JAWA BARAT</td>
      <td>TENJOLAYA</td>
    </tr>
  </tbody>
</table>
</div>



Berdasarkan tabel di atas, kita dapat melihat beberapa fakta untuk kasus  nomor kontak (Phone) yang double:
- Identitas (nama dan id number) berbeda. Contoh: Nomor kontak 6281585901341 dengan nama SYAHRIAN dan Ripai.
- Semua identitas sama sebenarnya, baik nama maupun id number. Namun, standar penulisan namanya berbeda; ada yang kapital semua, ada yang tidak. Contoh: Andi Maulanan Imam/ANDI MAULANAN IMAM. Mungkin untuk menghindari yang seperti ini bisa dibuat seragam upper atau lower case?
- Nama sebenarnya sama tapi standar penulisan berbeda, id number berbeda. Contoh: Solehudin/SOLEHUDIN
- Nama berbeda, id number sama. Contoh: 6285779380756 dengan nama Surya Mad Uhi dan Baban Subandi.
- Baris kedua dari nomor yang sama itu adalah Cust Fresh (bisa dilihat dari index), dengan komoditas dan Leads ID-nya kosong. 
- Kasus ini terjadi untuk data dengan region Tenjolaya 


### ID Number


```python
id_duplicate = data[data['ID Number'].isin(
    data['ID Number'].value_counts(
    )[data['ID Number'].value_counts() > 1].index.tolist()
    )
].sort_values(by='ID Number').reset_index(drop=True)
```

```python
display(HTML("""Total records yang tidak ada ID Number-nya sebanyak {:,}.
    ID Number juga ada kasus duplikat.
    Terdapat {} ID Number yang memiliki baris > 1. Contoh:
    """.format(
        data[data['ID Number'].isnull()].shape[0],
        id_duplicate['ID Number'].nunique()
)))
```


Total records yang tidak ada ID Number-nya sebanyak 857.
    ID Number juga ada kasus duplikat.
    Terdapat 39 ID Number yang memiliki baris > 1. Contoh:




```python
id_duplicate.iloc[:,1:12].head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Cust Feeder</th>
      <th>Cust Feedfund</th>
      <th>Cust Fresh</th>
      <th>Cust Regmem</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Nama</th>
      <th>Phone</th>
      <th>Province</th>
      <th>Region</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1308142711760002</td>
      <td>mas</td>
      <td>511f38qd8jjr</td>
      <td>Hasrinaldi</td>
      <td>6281312157627</td>
      <td>SUMATERA BARAT</td>
      <td>KABUPATEN PASAMAN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1308142711760002</td>
      <td>mas</td>
      <td>511f38qd8jjk</td>
      <td>M Khairul Amri</td>
      <td>6281266106123</td>
      <td>SUMATERA BARAT</td>
      <td>KABUPATEN PASAMAN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1308172006690001</td>
      <td>nila</td>
      <td>511pjpzn8jq6</td>
      <td>Hji Warzel</td>
      <td>6282388391144</td>
      <td>SUMATERA BARAT</td>
      <td>KABUPATEN PASAMAN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1308172006690001</td>
      <td>patin</td>
      <td>511p35sm6gup</td>
      <td>Saidi</td>
      <td>6281351727027</td>
      <td>KALIMANTAN SELATAN</td>
      <td>KABUPATEN HULU SUNGAI UTARA</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1401050212790003</td>
      <td>lele</td>
      <td>511pjmdqrftr</td>
      <td>Abdul Halim</td>
      <td>62523772032</td>
      <td>RIAU</td>
      <td>KABUPATEN KAMPAR</td>
    </tr>
  </tbody>
</table>
</div>




```python
id_duplicate.iloc[:,1:12].to_csv('/content/drive/My Drive/02 Engineering/Data Science/Notebooks/Datasets/data_id_number_duplicate.csv')
```
Daftar lengkap bisa dilihat di [sini](https://drive.google.com/file/d/1-CUmcxGzx7X_iUglCXQkkesB9juL6dLq/view?usp=sharing).

Dari berbagai kasus ID Number duplikat ini, penulis mengamati bahwa:
- Terdapat beberapa contoh dengan ID Number sama tetapi semua/beberapa/salah satu dari atribut Nama, Phone, Leads ID, Komoditas, Region-nya berbeda. 
  - Contoh:
    - ID Number 1308172006690001 ada 2 records memiliki atribut lain yang beda semua. 
    - ID Number 3201171506590015 namanya beda, tapi lainnya sama (Leads ID salah satunya null). 
    - ID Number 6308080810800003 banyak sekali recordsnya, dan Nama serta Phone-nya berbeda, region beragam. 
  - Pertanyaan: Untuk contoh yang ini mungkin karena customers memang memiliki banyak kolam, ya? Entah di region yang sama atau beda. Dan atribut Nama, Phone merujuk pada penanggung jawab di kolam tersebut, bukan customernya? 

- Sepertinya perlu menekankan untuk mencatat dengan standar penulisan yang sama dan teliti menuliskannya. Baik penentuan kapital semua atau tidak, nama depan saja atau nama lengkap, cek apakah nomor teleponnya sudah benar, kekonsistenan, dan sebagainya. Hal ini karena ditemukan berbagai kasus nama tidak seragam, padahal sepertinya merujuk pada orang yang sama (berdasarkan ID Number dan atribut lainnya). 
  - Contoh:
    - Kapital semua atau enggak? --> FAUZI vs Fauzi. 
        - Rekomendasi: Kalau sebenarnya perbedaan ini terjadi karena perbedaan pemrosesan data, misalnya data customer feeder saat query dibuat upper case, berarti sebaiknya diseragamkan semua juga untuk business unit lainnya.
    - Nama lengkap? --> SASONGKO CAHYA KARTIKA vs Sasongko, Tayadi vs TAYADI BN TARSIM (BN = bin? kalau iya, bahkan ini ada yang dikasih bin-nya)
    - Pakai spasi atau pakai 'H'? --> BUNCHIANG vs Bun Ciang
    - Nomor HP kurang angka --> 62523772032 vs 6285237722032, 628527190447 vs 6285271904447
  - Mengapa penting? Karena bisa saja seharusnya customer itu customer yang memang sama tetapi dianggap berbeda. Akibatnya, jumlah customer business unit tertentu kurang tepat. Misal, pada data customer lintas BU ini Rezki seolah menjadi customer berbeda karena nomor HP kurang satu angka; yang satunya jadi Rezki si customer Feedfund(+member), dan satunya Feeder. Padahal kemungkinan Rezki adalah customer 2 business unit. Catatan: kasus Rezki ini informasi identitas lainnya sama. Dugaan penulis ini terjadi karena di data customer business unit feedfund dengan feeder berbeda nomor kontaknya (selisih satu angka). Contoh lainnya Andi Imam Maulana yang namanya ada yang kapital semua dan tidak. Identitas lainnya sama (kecuali komoditas dan Leads ID ada yang null karena Fresh memang kurang lengkap). Andi terhitung sebagai 2 records berbeda, yang satunya Fresh dan satunya lagi Feedfund.







#### Leads ID


```python
print('Leads ID yang kosong untuk tiap business unit dan kombinasinya:')
data[(data['BU'] != 'Registered members, no BU') & (data['Leads ID'].isnull())]['BU'].value_counts()
```
    Leads ID yang kosong untuk tiap business unit dan kombinasinya:






    Feedfund           134
    Fresh               23
    Feedfund, Fresh     10
    Name: BU, dtype: int64



Feeder sepertinya sudah oke, lengkap!





```python
leads_duplicate = data[data['Leads ID'].isin(
    data['Leads ID'].value_counts(
    )[data['Leads ID'].value_counts() > 1].index.tolist()
    )
]
```

```python
display(HTML("""Sama halnya dengan Phone dan ID Number, penulis mencoba lihat
    duplikasi. Terdapat {} Leads ID yang memiliki duplikat. 
     Dan di bawah ini adalah records yang Leads ID-nya dimiliki lebih dari satu customer
    """.format(
        leads_duplicate['Leads ID'].nunique()
)))
```


Sama halnya dengan Phone dan ID Number, penulis mencoba lihat
    duplikasi. Terdapat 4 Leads ID yang memiliki duplikat. 
     Dan di bawah ini adalah records yang Leads ID-nya dimiliki lebih dari satu customer




```python
leads_duplicate.sort_values(by='Leads ID').iloc[:,1:12]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Cust Feeder</th>
      <th>Cust Feedfund</th>
      <th>Cust Fresh</th>
      <th>Cust Regmem</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Nama</th>
      <th>Phone</th>
      <th>Province</th>
      <th>Region</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>439</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>675592935322000</td>
      <td>NaN</td>
      <td>511f3c6wf4ew</td>
      <td>Muhammad Ade Hendrik</td>
      <td>6281363803089</td>
      <td>LAMPUNG</td>
      <td>KOTA BANDAR LAMPUNG</td>
    </tr>
    <tr>
      <th>804</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>913768610047000</td>
      <td>NaN</td>
      <td>511f3c6wf4ew</td>
      <td>PT. GROWELL FARM INDONESIA - LAMPUNG</td>
      <td>6282279103459</td>
      <td>LAMPUNG</td>
      <td>KABUPATEN PESAWARAN</td>
    </tr>
    <tr>
      <th>193</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6208011705810002</td>
      <td>NaN</td>
      <td>511f3uzx5vkp</td>
      <td>Dwi Setiyo Rahadi</td>
      <td>6281251033811</td>
      <td>JAWA TENGAH</td>
      <td>KABUPATEN PURWOREJO</td>
    </tr>
    <tr>
      <th>903</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3321030807940005</td>
      <td>NaN</td>
      <td>511f3uzx5vkp</td>
      <td>Gilang Izzanudin</td>
      <td>6282372660912</td>
      <td>JAWA TENGAH</td>
      <td>KABUPATEN PURWOREJO</td>
    </tr>
    <tr>
      <th>722</th>
      <td>1.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>1801101508810001</td>
      <td>lele</td>
      <td>511pj44iv4zf</td>
      <td>WARTO</td>
      <td>6282176759281</td>
      <td>LAMPUNG</td>
      <td>KABUPATEN LAMPUNG SELATAN</td>
    </tr>
    <tr>
      <th>723</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1801101204750005</td>
      <td>lele</td>
      <td>511pj44iv4zf</td>
      <td>Waluyo</td>
      <td>6282177340103</td>
      <td>LAMPUNG</td>
      <td>KABUPATEN LAMPUNG SELATAN</td>
    </tr>
    <tr>
      <th>1242</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>712679133014000</td>
      <td>NaN</td>
      <td>511ubad9kcdh</td>
      <td>PT. NOERWY AQUA FARM - SUKABUMI</td>
      <td>6285624573034</td>
      <td>JAWA BARAT</td>
      <td>KABUPATEN SUKABUMI</td>
    </tr>
    <tr>
      <th>1285</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3202241206870005</td>
      <td>NaN</td>
      <td>511ubad9kcdh</td>
      <td>Herman</td>
      <td>6285703032648</td>
      <td>JAWA BARAT</td>
      <td>KABUPATEN SUKABUMI</td>
    </tr>
  </tbody>
</table>
</div>



Pertanyaan *(Ini dicatat dulu ya di sini)*: 

Kurang yakin mengenai ini kenapa terjadinya. Apakah Leads ID memang tidak unique? Tidak 1-1 untuk tiap customer?

Sudah cek di data `leads_all` dan `customers`, misalnya leads ID 511f3c6wf4ew. Hasilnya adalah di `leads_all` ada satu baris dengan nama leads Ade Hendrik Chen Ming. Lalu di `customers` ada 2 baris dengan nama berbeda; Muhammad Ade Hendrik (company_name PT. Delivra Sinar Sentosa) dan PT. GROWELL FARM INDONESIA - LAMPUNG (company_name-nya sama dengan nama). Timestamp pembuatannya beda beberapa bulan. Kalau dilihat dari namanya, ini lebih tepat milik yang namanya ada Ade Hendrik-nya meski penulisan  berbeda.

Leads ID digenerate berdasarkan/dari...?

### Kualitas data: Kelengkapan data


```python
# df_null_per_bu_region = df[df['BU'] != 'Registered members, no BU'].groupby(
df_null_per_bu_region = df.groupby(
    ['BU', 'Region']).agg(
      {
        'ID Number': lambda x: x.isnull().sum(),
        'Komoditas': lambda x: x.isnull().sum(),
        'Leads ID': lambda x: x.isnull().sum(),
        'Row Number': pd.Series.count 
      }
).rename(
    columns={
        'Row Number': 'Total Records' # ini total records, bukan yang null aja
    }
).reset_index()
```

```python
# hitung persentase null per kolom
## ID Number
df_null_per_bu_region['% ID Number'] = round(
      df_null_per_bu_region['ID Number'] / df_null_per_bu_region['Total Records'] * 100, 2
    )

## Komoditas
df_null_per_bu_region['% Komoditas'] = round(
      df_null_per_bu_region['Komoditas'] / df_null_per_bu_region['Total Records'] * 100, 2
    )

## Leads ID
df_null_per_bu_region['% Leads ID'] = round(
      df_null_per_bu_region['Leads ID'] / df_null_per_bu_region['Total Records'] * 100, 2
    )
```
Seperti yang sudah dapat dilihat pada bagian sebelumnya (gambaran data), terdapat kolom yang isinya tidak lengkap. 

Pada bagian ini penulis memberikan daftar kelengkapan data berdasarkan business unit dan regionnya seperti tabel di bawah:


```python
def get_score(id, k):
  if (id < 25) and (k < 25):
    return 5
  elif (id >= 25 and id < 50) and (k < 25):
    return 4
  elif (id < 25) and (k >= 25 and k < 50):
    return 4
  elif (id >= 25 and id <= 50) and (k >= 25 and k <= 50):
    return 3
  elif (id >= 50) and (k < 50):
    return 3
  elif (id < 50) and (k >= 50):
    return 3
  elif (id >= 50 and id < 75) and (k >= 50 and k < 75):
    return 2
  elif (id < 75) and (k >= 75):
    return 2
  elif (id >= 75) and (k < 75):
    return 2
  elif (id >= 75) and (k >= 75):
    return 1
```

```python
df_null_per_bu_region['score'] = df_null_per_bu_region.apply(
    lambda x: get_score(x['% ID Number'], x['% Komoditas']), axis=1
    )
```

```python
df_null_per_bu_region = df_null_per_bu_region.sort_values(
    by=['BU','score', 'Total Records'], 
    ascending=[True,True,False]
    ).reset_index(drop=True)
```

```python
df_null_per_bu_region.to_csv('/content/drive/My Drive/02 Engineering/Data Science/Notebooks/Datasets/kelengkapan_data_per_bu_per_region.csv')
```

```python
df_null_per_bu_region.groupby('BU').head(1).reset_index(drop=True).sort_values(by='BU')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>BU</th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
      <th>score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Feeder</td>
      <td>KABUPATEN CILACAP</td>
      <td>7</td>
      <td>8</td>
      <td>0</td>
      <td>8</td>
      <td>87.5</td>
      <td>100.0</td>
      <td>0.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Feeder, Feedfund</td>
      <td>TENJOLAYA</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Feeder, Feedfund, Fresh</td>
      <td>KABUPATEN LAMPUNG SELATAN</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>4</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Feeder, Fresh</td>
      <td>KOTA CIREBON</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Feedfund</td>
      <td>KABUPATEN TULUNGAGUNG</td>
      <td>0</td>
      <td>0</td>
      <td>21</td>
      <td>23</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>91.3</td>
      <td>5</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Feedfund, Fresh</td>
      <td>KABUPATEN SUBANG</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>4</td>
      <td>0.0</td>
      <td>25.0</td>
      <td>0.0</td>
      <td>4</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Fresh</td>
      <td>KABUPATEN INDRAMAYU</td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Registered members, no BU</td>
      <td>KOTA BLITAR</td>
      <td>31</td>
      <td>31</td>
      <td>31</td>
      <td>31</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



Dalam pemberian score, penulis hanya mempertimbangkan ID Number dan Komoditas. Aturan yang diterapkan dalam pembuatan score adalah sebagai berikut:
- Score 5: ketidaklengkapan id dan komoditas < 25%  
- Score 4: ketidaklengkapan id antara 25% hingga 49%25%, komoditas < 25%
- Score 4: ketidaklengkapan id < 25%, komoditas antara 25% hingga 49%
- Score 3: ketidaklengkapan id dan komoditas antara 25% - 50%
- Score 3: ketidaklengkapan id antara 25% - 50%, komoditas < 50%
- Score 3: ketidaklengkapan id < 50%, komoditas 25% - 50%
- Score 2: ketidaklengkapan id dan komoditas antara 50% hingga 75%
- Score 2: ketidaklengkapan id < 75%, komoditas >= 75%
- Score 2: ketidaklengkapan id >= 75%, komoditas < 75%
- Score 1: ketidaklengkapan id dan komoditas di atas 75%

Semakin kecil scorenya, berarti semakin tidak lengkap. Semakin besar score, kelengkapan data bisa dibilang semakin baik.

Daftar kelengkapan data per business unit per region dapat di akses [di sini](https://drive.google.com/file/d/1-DL1Ph1jFK9hYDJRxbuF1wTYoujfzmpL/view?usp=sharing). 

Sekarang, mari kita lihat beberapa region untuk masing-masing score dan business unit (termasuk irisannya), berurut dari total records/customer terbanyak! Dari berbagai tabel di bawah, mungkin dapat menjadi rekomendasi tim operasional untuk memperhatikan kelengkapan datanya dengan prioritas region yang memiliki customer lebih banyak.

_Catatan_: 
- _Jika tabel di bawah ada yang kosong berarti tidak ada data pada score dan business unit tersebut_
- _Total records = total baris = total customers. Tiap baris dianggap sebagai customer._


```python
# feeder = df_null_per_bu_region[df_null_per_bu_region['BU'].str.contains('Feeder')] ## nggak jadi deh. single aja.
# feedfund = df_null_per_bu_region[df_null_per_bu_region['BU'].str.contains('Feedfund')]
# fresh = df_null_per_bu_region[df_null_per_bu_region['BU'].str.contains('Fresh')]

feeder = df_null_per_bu_region.query('BU=="Feeder"')
feedfund = df_null_per_bu_region.query('BU=="Feedfund"')
fresh = df_null_per_bu_region.query('BU=="Fresh"')
```
##### Score 1


```python
display(HTML('<h4 style="color: red">Feeder</h4>'))
feeder.query('score==1').head(10).iloc[:,1:9]
```


<h4 style="color: red">Feeder</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN CILACAP</td>
      <td>7</td>
      <td>8</td>
      <td>0</td>
      <td>8</td>
      <td>87.5</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KOTA BANDUNG</td>
      <td>7</td>
      <td>7</td>
      <td>0</td>
      <td>8</td>
      <td>87.5</td>
      <td>87.5</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>KABUPATEN LAMPUNG TENGAH</td>
      <td>6</td>
      <td>6</td>
      <td>0</td>
      <td>6</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KOTA DEPOK</td>
      <td>6</td>
      <td>6</td>
      <td>0</td>
      <td>6</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KABUPATEN PASURUAN</td>
      <td>5</td>
      <td>4</td>
      <td>0</td>
      <td>5</td>
      <td>100.0</td>
      <td>80.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>KABUPATEN JEMBRANA</td>
      <td>4</td>
      <td>4</td>
      <td>0</td>
      <td>4</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>KABUPATEN KLATEN</td>
      <td>4</td>
      <td>4</td>
      <td>0</td>
      <td>4</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>KABUPATEN LAMPUNG TIMUR</td>
      <td>3</td>
      <td>4</td>
      <td>0</td>
      <td>4</td>
      <td>75.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>KABUPATEN PANGANDARAN</td>
      <td>3</td>
      <td>4</td>
      <td>0</td>
      <td>4</td>
      <td>75.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>KABUPATEN SITUBONDO</td>
      <td>3</td>
      <td>4</td>
      <td>0</td>
      <td>4</td>
      <td>75.0</td>
      <td>100.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">Fresh</h4>'))
fresh.query('score==1').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Fresh</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN INDRAMAYU</td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KABUPATEN LOMBOK BARAT</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">FeedFund</h4>'))
feedfund.query('score==1').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">FeedFund</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



##### Score 2


```python
display(HTML('<h4 style="color: red">Feeder</h4>'))
feeder.query('score==2').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Feeder</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN LAMPUNG SELATAN</td>
      <td>10</td>
      <td>14</td>
      <td>0</td>
      <td>17</td>
      <td>58.82</td>
      <td>82.35</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KABUPATEN KARAWANG</td>
      <td>5</td>
      <td>6</td>
      <td>0</td>
      <td>9</td>
      <td>55.56</td>
      <td>66.67</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>KABUPATEN PANDEGLANG</td>
      <td>5</td>
      <td>9</td>
      <td>0</td>
      <td>9</td>
      <td>55.56</td>
      <td>100.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KABUPATEN PESAWARAN</td>
      <td>4</td>
      <td>7</td>
      <td>0</td>
      <td>8</td>
      <td>50.00</td>
      <td>87.50</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KABUPATEN CIANJUR</td>
      <td>4</td>
      <td>5</td>
      <td>0</td>
      <td>7</td>
      <td>57.14</td>
      <td>71.43</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>KOTA BANDAR LAMPUNG</td>
      <td>5</td>
      <td>7</td>
      <td>0</td>
      <td>7</td>
      <td>71.43</td>
      <td>100.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>KABUPATEN BANTUL</td>
      <td>4</td>
      <td>6</td>
      <td>0</td>
      <td>6</td>
      <td>66.67</td>
      <td>100.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>KABUPATEN LOMBOK TENGAH</td>
      <td>4</td>
      <td>5</td>
      <td>0</td>
      <td>6</td>
      <td>66.67</td>
      <td>83.33</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>KABUPATEN SLEMAN</td>
      <td>4</td>
      <td>5</td>
      <td>0</td>
      <td>6</td>
      <td>66.67</td>
      <td>83.33</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>KOTA SURABAYA</td>
      <td>4</td>
      <td>6</td>
      <td>0</td>
      <td>6</td>
      <td>66.67</td>
      <td>100.00</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">Fresh</h4>'))
fresh.query('score==2').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Fresh</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">FeedFund</h4>'))
feedfund.query('score==2').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">FeedFund</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



##### Score 3


```python
display(HTML('<h4 style="color: red">Feeder</h4>'))
feeder.query('score==3').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Feeder</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN INDRAMAYU</td>
      <td>39</td>
      <td>23</td>
      <td>0</td>
      <td>61</td>
      <td>63.93</td>
      <td>37.70</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KABUPATEN BANJAR</td>
      <td>15</td>
      <td>12</td>
      <td>0</td>
      <td>30</td>
      <td>50.00</td>
      <td>40.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>KABUPATEN TASIKMALAYA</td>
      <td>13</td>
      <td>20</td>
      <td>0</td>
      <td>30</td>
      <td>43.33</td>
      <td>66.67</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KABUPATEN TULUNGAGUNG</td>
      <td>12</td>
      <td>1</td>
      <td>0</td>
      <td>19</td>
      <td>63.16</td>
      <td>5.26</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KABUPATEN CIREBON</td>
      <td>12</td>
      <td>4</td>
      <td>0</td>
      <td>16</td>
      <td>75.00</td>
      <td>25.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>KABUPATEN LOMBOK TIMUR</td>
      <td>1</td>
      <td>9</td>
      <td>0</td>
      <td>16</td>
      <td>6.25</td>
      <td>56.25</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>KABUPATEN PURWOREJO</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>14</td>
      <td>0.00</td>
      <td>100.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>KABUPATEN BANGKALAN</td>
      <td>3</td>
      <td>11</td>
      <td>0</td>
      <td>12</td>
      <td>25.00</td>
      <td>91.67</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>KABUPATEN SUKABUMI</td>
      <td>2</td>
      <td>10</td>
      <td>0</td>
      <td>11</td>
      <td>18.18</td>
      <td>90.91</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>KABUPATEN BANYU ASIN</td>
      <td>3</td>
      <td>5</td>
      <td>0</td>
      <td>10</td>
      <td>30.00</td>
      <td>50.00</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">Fresh</h4>'))
fresh.query('score==3').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Fresh</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>TENJOLAYA</td>
      <td>0</td>
      <td>9</td>
      <td>9</td>
      <td>9</td>
      <td>0.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KABUPATEN LAMPUNG SELATAN</td>
      <td>1</td>
      <td>5</td>
      <td>5</td>
      <td>5</td>
      <td>20.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>KOTA CIREBON</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>0.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KABUPATEN MUSI RAWAS UTARA</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KOTA TASIKMALAYA</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0.0</td>
      <td>100.0</td>
      <td>100.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">FeedFund</h4>'))
feedfund.query('score==3').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">FeedFund</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



##### Score 4


```python
display(HTML('<h4 style="color: red">Feeder</h4>'))
feeder.query('score==4').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Feeder</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN BOGOR</td>
      <td>17</td>
      <td>8</td>
      <td>0</td>
      <td>55</td>
      <td>30.91</td>
      <td>14.55</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KABUPATEN SUBANG</td>
      <td>19</td>
      <td>8</td>
      <td>0</td>
      <td>39</td>
      <td>48.72</td>
      <td>20.51</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>KOTA PALEMBANG</td>
      <td>1</td>
      <td>3</td>
      <td>0</td>
      <td>9</td>
      <td>11.11</td>
      <td>33.33</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KABUPATEN BLITAR</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>4</td>
      <td>0.00</td>
      <td>25.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KABUPATEN PRINGSEWU</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>4</td>
      <td>0.00</td>
      <td>25.00</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">Fresh</h4>'))
fresh.query('score==4').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Fresh</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">FeedFund</h4>'))
feedfund.query('score==4').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">FeedFund</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



##### Score 5


```python
display(HTML('<h4 style="color: red">Feeder</h4>'))
feeder.query('score==5').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Feeder</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN HULU SUNGAI UTARA</td>
      <td>1</td>
      <td>3</td>
      <td>0</td>
      <td>18</td>
      <td>5.56</td>
      <td>16.67</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KABUPATEN MUSI RAWAS</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>13</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>KABUPATEN BENGKULU UTARA</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>8</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KABUPATEN KAMPAR</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>8</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KABUPATEN NGAWI</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
      <td>14.29</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>KABUPATEN PASAMAN</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>KABUPATEN TABALONG</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>KABUPATEN MUSI RAWAS UTARA</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>4</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>KABUPATEN JOMBANG</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>KOTA TASIKMALAYA</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>0.00</td>
      <td>0.00</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">Fresh</h4>'))
fresh.query('score==5').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">Fresh</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>




```python
display(HTML('<h4 style="color: red">FeedFund</h4>'))
feedfund.query('score==5').head(10).reset_index(drop=True).iloc[:,1:9]
```


<h4 style="color: red">FeedFund</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KABUPATEN TULUNGAGUNG</td>
      <td>0</td>
      <td>0</td>
      <td>21</td>
      <td>23</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>91.30</td>
    </tr>
    <tr>
      <th>1</th>
      <td>KOTA CIREBON</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>21</td>
      <td>0.0</td>
      <td>14.29</td>
      <td>19.05</td>
    </tr>
    <tr>
      <th>2</th>
      <td>TENJOLAYA</td>
      <td>0</td>
      <td>1</td>
      <td>5</td>
      <td>21</td>
      <td>0.0</td>
      <td>4.76</td>
      <td>23.81</td>
    </tr>
    <tr>
      <th>3</th>
      <td>KABUPATEN KAMPAR</td>
      <td>0</td>
      <td>0</td>
      <td>20</td>
      <td>20</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>100.00</td>
    </tr>
    <tr>
      <th>4</th>
      <td>KABUPATEN SUBANG</td>
      <td>0</td>
      <td>0</td>
      <td>13</td>
      <td>14</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>92.86</td>
    </tr>
    <tr>
      <th>5</th>
      <td>KABUPATEN MUSI BANYU ASIN</td>
      <td>0</td>
      <td>0</td>
      <td>11</td>
      <td>11</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>100.00</td>
    </tr>
    <tr>
      <th>6</th>
      <td>KOTA TASIKMALAYA</td>
      <td>0</td>
      <td>0</td>
      <td>10</td>
      <td>10</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>100.00</td>
    </tr>
    <tr>
      <th>7</th>
      <td>KABUPATEN PRINGSEWU</td>
      <td>0</td>
      <td>0</td>
      <td>9</td>
      <td>9</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>100.00</td>
    </tr>
    <tr>
      <th>8</th>
      <td>KOTA BLITAR</td>
      <td>0</td>
      <td>0</td>
      <td>9</td>
      <td>9</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>100.00</td>
    </tr>
    <tr>
      <th>9</th>
      <td>KABUPATEN LAMPUNG SELATAN</td>
      <td>0</td>
      <td>0</td>
      <td>6</td>
      <td>6</td>
      <td>0.0</td>
      <td>0.00</td>
      <td>100.00</td>
    </tr>
  </tbody>
</table>
</div>



##### Lain-lain


```python
c = df_null_per_bu_region.query("BU in (['Feeder, Feedfund', 'Feeder, Feedfund, Fresh', 'Feeder, Fresh', 'Feedfund, Fresh'])")
display(HTML('<h4 style="color: red">Gabungan business unit dengan score < 5</h4>'))
c.query('score<5').head(10).reset_index(drop=True)
```


<h4 style="color: red">Gabungan business unit dengan score < 5</h4>






<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>BU</th>
      <th>Region</th>
      <th>ID Number</th>
      <th>Komoditas</th>
      <th>Leads ID</th>
      <th>Total Records</th>
      <th>% ID Number</th>
      <th>% Komoditas</th>
      <th>% Leads ID</th>
      <th>score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Feeder, Fresh</td>
      <td>KOTA CIREBON</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Feeder, Fresh</td>
      <td>KABUPATEN SUKABUMI</td>
      <td>0</td>
      <td>2</td>
      <td>0</td>
      <td>2</td>
      <td>0.0</td>
      <td>100.0</td>
      <td>0.0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Feedfund, Fresh</td>
      <td>KABUPATEN SUBANG</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>4</td>
      <td>0.0</td>
      <td>25.0</td>
      <td>0.0</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
</div>



### Eksplorasi lainnya


```python
df = df.drop(columns=['nan'])
```

```python
k = df.iloc[:,12:25].columns.tolist()
komoditas_per_region = df.groupby(['Region'])[k].sum().reset_index()
komoditas_per_region_melt = komoditas_per_region.melt(id_vars=['Region'], 
        var_name='Komoditas', 
        value_name='Total')

top_region = df['Region'].value_counts().head(10).index.tolist()
komoditas_per_10_region = df[df['Region'].isin(top_region)].groupby(['Region'])[k].sum()
```

```python
# komoditas_per_10_region
```

```python
komoditas_per_10_region = komoditas_per_10_region.reindex(kmdts.index, axis=1)
```

```python
komoditas_per_10_region.plot.barh(
    stacked=True,
    colormap='tab20',
    # ax=ax
)
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.08),
          fancybox=True, shadow=True, ncol=5)
# plt.legend(loc='lower left', fontsize=11)

plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_98_0.png)


_Stack paling bawah/kiri ke kanan urut dari ikan komoditas tertinggi sampai terendah (nila -> lele -> patin ->...-> kakap)_

Dari stacked bar chart pun terlihat bahwa banyak komoditas yang kosong karena jumlahnya berkurang. 
Selain itu, kita dapat melihat ada sedikit perbedaan komoditas utama antara region di Jawa Timur dengan Jawa Barat, yaitu ikan nila yang merupakan komoditas tertinggi. Pada region di Jawa Timur yang termasuk 10 besar ini ikan nila hanya 1 di Kab. Tulungagung, begitu juga dengan region di Sumatera Selatan yang tidak ada sama sekali. Namun, hal ini masih diragukan kevalidannya karena banyak komoditas yang tidak lengkap pada data, terutama Jawa Timur dan Sumatera Selatan yang persentase komoditas kosong > 50%.


```python
a = df.copy()
a['BU'] = a['BU'].str.replace('Registered members, no BU', 'Registered Member').str.replace(' ', '')
a["BU"]=a["BU"].str.split(",")
a = a.explode("BU").reset_index(drop=True)

# pd.concat([pd.Series(row['bandeng'], row['nila'], row['BU'].split(','))              
#                     for _, row in df.iterrows()]).reset_index()
```

```python
komoditas_bu = a[
                      a['BU'] != 'RegisteredMember'
                      ].groupby(['BU'])[k].sum()
# komoditas_per_bu = komoditas_per_bu.melt(id_vars=['BU'], 
#         var_name='Komoditas', 
#         value_name='Total')

komoditas_bu = komoditas_bu.reindex(kmdts.index, axis=1)
```

```python
komoditas_per_bu = df[
                      df['BU'] != 'Registered members, no BU'
                      ].groupby(['BU'])[k].sum()
# komoditas_per_bu = komoditas_per_bu.melt(id_vars=['BU'], 
#         var_name='Komoditas', 
#         value_name='Total')

komoditas_per_bu = komoditas_per_bu.reindex(kmdts.index, axis=1)
```

```python
ax = komoditas_bu.plot.barh(
    stacked=True,
    colormap='tab20',
    # ax=ax
)
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.08),
          fancybox=True, shadow=True, ncol=5)
# plt.legend(loc='lower left', fontsize=11)


plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_103_0.png)


Jika tidak melihat irisan atau lintas BU, data komoditas seolah sudah cukup baik, sudah cukup lengkap untuk masing-masing business unit. _Apakah benar?_



```python
ax = komoditas_per_bu.plot.barh(
    stacked=True,
    colormap='tab20',
    # ax=ax
)
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.08),
          fancybox=True, shadow=True, ncol=5)
# plt.legend(loc='lower left', fontsize=11)


plt.ylabel('')
sns.despine()
plt.show()
```


![png](images/output_105_0.png)


- Fresh tidak ada data komoditasnya. Yang membuat pada grafik sebelumnya Fresh terlihat komoditasnya adalah karena memperhitungkan irisannya juga atau lintas business unit. Misalnya, Feedfund+Fresh. Ini berarti yang lengkap adalah data Feedfund.
- Feedfund dan fresh itu memang buat semua komoditas juga kan ya? Lihat di grup WA promosi Fresh ada produk udangnya. "Udang segar yang langsung dipanen dari Pembudidaya eFishery". Tapi di sini Fresh atau kombinasinya (Feeder+Fresh, Feedfund+Fresh, Feeder+Fresh+Feedfund) belum menangkap adanya komoditas udang. 
- Lain-lain:
  - Q: _"Farmers fresh kok dikit banget ya. Apa mungkin data fresh banyak yang kurang lengkap jadi nggak masuk ke raw?"_ 
  - A: Iya. Dari `fresh_supplier_leads` seharusnya ada > 100. Namun, karena datanya kurang lengkap (dalam hal ini terutama phone number), dan kita menyatukan datanya menggunakan itu, jadi pada data lintas BU ini hanya mencakup sekitar ~50%-60%-nya.
  - Q: _Irisan farmer feeder dan fresh dikit banget juga, malah lebih banyak gabungan 3 BU. Apakah cross-selling feeder-fresh di sini kurang diminati? (i.e Petani yang mampu beli feeder udah nggak pusing cari market, ngga tertarik fresh)_ 
  - A: Belum punya dasar yang cukup untuk tau kurang diminatinya. Dugaan penulis, lagi-lagi ini masalah datanya. Kenapa feedfund-fresh dan gabungan 3 BU lebih banyak? Kemungkinan karena feedfund kelengkapan datanya sudah cukup baik, sementara feeder dan fresh sama-sama masih kurang. Bisa jadi itulah penyebab feeder dan fresh seolah banyak yang 'gagal bertemu', padahal mungkin saja sebenarnya angka feeder-fresh bisa lebih banyak lagi (tapi bisa jadi juga malah 3 BU yang lebih banyak). Untuk tau diminati tidaknya, sepertinya butuh informasi petani lebih banyak selain di data raw customer lintas bu. Contohnya informasi panen, sudah beli feeder berapa unit atau lainnya untuk tau behaviour petani, atau keuangan/penghasilan (?). Mari diskusi!


## Akhirnya

### Kesimpulan
*Ayo ayo kita tim data semangat hehe :D*

### Saran dan Rekomendasi

Seperti yang sudah disebut di atas, beberapa hal yang perlu diperhatikan adalah:
- Standar penulisan, kekonsistenan, ketelitian. Contohnya:
  - Tim operasional/lapangan: dalam pencatatan apakah menuliskan nama lengkap atau nama depan saja (nama sesuai kartu identitas lebih baik), spelling-nya apakah sudah tepat, nomor kontak bisa diperiksa lagi agar tidak keliru, nomor identitas sudah tercatat apa belum.
  - Tim data: saat mengolah data bisa disamakan aturannya agar konsisten. Misal nama dibuat upper case untuk di data customer semua business unit. 
- Komoditas perlu dibersihkan dulu oleh tim data jika tidak memungkinkan untuk mengubah form atau bentuk input dari tim operasional atau lapangan. Karena free text, jadi penulisannya beda-beda. 

Saran dan rekomendasi sudah disebut di atas untuk masing-masing bagian. Untuk mendukung menuju raw data yang lebih tepat, berikut beberapa dokumen yang diharapkan bisa membantu:
- [Score kelengkapan data per business unit per region](https://drive.google.com/file/d/1-DL1Ph1jFK9hYDJRxbuF1wTYoujfzmpL/view?usp=sharing). Tim bisa mempertimbangkan region mana yang jadi prioritas untuk membuat datanya lebih lengkap. 
- [Data ID Number yang duplikat](https://drive.google.com/file/d/1-CUmcxGzx7X_iUglCXQkkesB9juL6dLq/view?usp=sharing)
- [Raw data per business unit yang nomor telepon atau nomor identitasnya kosong](https://docs.google.com/spreadsheets/d/1AUOCTtwmo8Dn4fNp8YHEgfleJAmKj17vMEf6FiHtR4k/edit?usp=sharing) (per 2 Juli 2020). Untuk feeder cukup banyak yang identitasnya kosong, jadi jika ingin melihat yang nomor teleponnya saja yang kosong bisa pakai filter, ya.

_Untuk raw data per bu yang belum lengkap ini masih download manual. Apa mending bikin questionnya?_


## Referensi

##### ---


```python
#### Percobaan Clustering (belum tepat, silakan di-hide)

# Penulis mencoba melakukan clustering berdasarkan letak geografis dan preferensi pengguna (komoditas dan business unit).
```

```python
# z = df[['BU', 'Komoditas', 'Region', 'Province']]
# temp = z.dropna()
```

```python
# hapus
#  temp['Region'] = temp['Region'].str.replace('KABUPATEN', '').str.replace('KOTA', '')
```

```python
# teks = temp['BU'] + ' ' + temp['Komoditas'] + ' ' + temp['Region'] 
```

```python
# # coba pakai bag of words ya
# vct = CountVectorizer()

# bow = vct.fit_transform(teks.values)

# feature_names = vct.get_feature_names()
# d = pd.DataFrame(bow.toarray(), columns=feature_names)
```

```python
## hapus
# vectorizer = TfidfVectorizer()
# tf_idf = vectorizer.fit_transform(teks.values)

# tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
# tfidf = pd.DataFrame(columns=['idf']).from_dict(dict(tfidf), orient='index')
# tfidf.columns = ['idf']
```

```python
# svd = TruncatedSVD(n_components=2, random_state=RANDOM_STATE)
# svd_ = svd.fit_transform(bow)
```

```python
# colormap = np.array(
#     ["#cfbdce","#d07d3c",
#      "#6d8dca", "#69de53", "#723bca", "#c3e14c", "#c84dc9", "#68af4e", "#6e6cd5","#e3be38",
#      "#4e2d7c", "#5fdfa8", "#d34690", "#3f6d31", "#d44427", "#7fcdd8", "#cb4053", "#5e9981",
#      "#803a62", "#9b9e39", "#c88cca", "#e1c37b", "#34223b", "#bdd8a3", "#6e3326","#d27c88",
#      "#7d6d33", "#36422b", "#b68f79","#52697d"]
# )

# _cc = []
# _colmap = []

# for i in np.arange(0,len(colormap)):
#     _cc.append(i)
#     _colmap.append(colormap[i])
    
# colormap_df = pd.DataFrame({
#     'cluster_code': _cc,
#     'color': _colmap}
# )
```

```python
# from sklearn.cluster import DBSCAN
# from sklearn import metrics
# import bokeh.plotting as bp
# from bokeh.models import HoverTool, BoxSelectTool
# from bokeh.plotting import figure, show, output_notebook
# from bokeh.palettes import d3
# import bokeh.models as bmo

# output_notebook()

# db = DBSCAN(eps=0.2, min_samples=5).fit(svd_)
# core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
# core_samples_mask[db.core_sample_indices_] = True
# labels = db.labels_

# # Number of clusters in labels, ignoring noise if present.
# n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
# n_noise_ = list(labels).count(-1)

# print('Estimated number of clusters: %d' % n_clusters_)
# print('Estimated number of noise points: %d' % n_noise_)

# # fig, ax = plt.subplots(figsize=(9, 7))
# # plt.scatter(svd_[:, 0], svd_[:, 1], c=labels, s=50, cmap='viridis')


# dbscan_df = pd.DataFrame(svd_, columns=['x', 'y'])
# dbscan_df['cluster'] = labels
# dbscan_df['BU'] = df['BU']
# dbscan_df['Komoditas'] = df['Komoditas']
# dbscan_df['Region'] = df['Region']
# dbscan_df['Province'] = df['Province']

# dbscan_df = dbscan_df.merge(
#     colormap_df,
#     how='left',
#     left_on='cluster',
#     right_on='cluster_code'
# ).drop(labels=['cluster_code'], axis=1)
```

```python
# plot_db = bp.figure(plot_width=800, plot_height=600, title="Clustering",
#     tools="pan,wheel_zoom,box_zoom,reset,hover,previewsave",
#     x_axis_type=None, y_axis_type=None, min_border=1)

# plot_db.scatter(x='x', y='y',
#                     color='color',
#                     source=dbscan_df)
# hover = plot_db.select(dict(type=HoverTool))
# hover.tooltips={"BU": "@BU", "Komoditas": "@Komoditas", "Region": "@Region", "cluster":"@cluster"}
# show(plot_db)
```

```python
# *Maaf bapak-bapak dan ibu-ibu, saya masih bingung menginterpretasikannya... Mungkin approach-nya kurang tepat juga*

# *Ada saran lain dari Pak Ali untuk pakai NMF, nah ini belum dicoba. Sedang belajar agar mengerti.*


# Update 2020-07-06: Hasil clusteringnya berubah... Karena datanya berubah jg mungkin (?)
```

```python
# import matplotlib.colors as mcolors

# colmap = list(mcolors.cnames.values())
# clcode = []

# for i in range(0,len(colmap)):
#   clcode.append(i)

# colormap_df2 = pd.DataFrame({
#     'cluster_code': clcode,
#     'color': colmap}
# )

# dbscan_df2 = dbscan_df

# # label encoder
# from sklearn.preprocessing import LabelEncoder

# labelencoder = LabelEncoder()
# dbscan_df2['Province'] = dbscan_df2['Province'].fillna('-')
# dbscan_df2['Province_Code'] = labelencoder.fit_transform(dbscan_df2['Province'])

# dbscan_df2 = dbscan_df2.merge(
#     colormap_df2,
#     how='left',
#     left_on='Province_Code',
#     right_on='cluster_code'
# ).drop(labels=['cluster_code'], axis=1)
```

```python
# plot_db = bp.figure(plot_width=800, plot_height=600, title="Clustering",
#     tools="pan,wheel_zoom,box_zoom,reset,hover,previewsave",
#     x_axis_type=None, y_axis_type=None, min_border=1)

# plot_db.scatter(x='x', y='y',
#                     color='color_y',
#                     source=dbscan_df2)
# hover = plot_db.select(dict(type=HoverTool))
# hover.tooltips={"BU": "@BU", "Komoditas": "@Komoditas", "Province": "@Province", "cluster":"@cluster"}
# show(plot_db)
```
##### Catatan ID Number (silakan bisa dilewati saja)

- Nama, Phone, Leads ID berbeda. Contoh ID Number 1308142711760002 dengan nama M Khairul Amri/Hasrinaldi. (Cust Feeder dua-duanya, Komoditas sama, Region sama)
- Nama, Phone, Leads ID, komoditas, region berbeda. ID Number, Province, Cust Feeder sama. Contoh: Hji Warzel/Saidi. Apakah ini memang identitas yang beda? Kenapa ID Numbernya bisa sama, ya?
- Nama sama, Phone selisih satu angka, misal di satunya 4447, yang satunya lagi 447. Region sama, Leads ID yang satunya null. Cust Feeder x Cust Feedfund+Fresh. Contoh: Abdul Halim dan Rezki.
- Nama standar penulisannya berbeda, Phone beda satu angka, misal yang satunya angka 7, yang satunya lagi angka 3. Leads ID yang satunya null. Komoditas beda tapi ada irisan (lele vs lele,patin). Region beda nama dikit (Kabupaten Musi Banyu Asin vs Kabupaten Banyu Asin). Cust Feeder vs Feedfund. Contoh: Fauzi/FAUZI
- Nama beda penulisan (BUNCHIANG vs Bun Ciang), Phone berbeda, Region beda (Musi Banyu Asin vs Banyu Asin), Komoditas dan Leads ID yang satunya null. Cust Feeder vs Feedfund
- Nama ada pemotongan (Nama lengkap vs nama depan), Phone berbeda, Region sama, Komoditas dan Leads ID yang satunya null. Contoh: SASONGKO CAHYA KARTIKA vs Sasongko (Feeder+Feedfund vs Fresh), Tayadi vs TAYADI BN TARSIM (Fresh vs Feedfund)
- Nama beda standar tulis, Phone beda, Region beda (Lampung vs Sukabumi), Komoditas dan Leads ID salah satunya null. Cust Feeder vs Feedfund+Fresh. Contoh: WARSONO/Warsono. Mungkin ini emang punya 2(?)
- Nama beda standar tulis, Phone beda, Region sama, Komoditas dan Leads ID salah satu null. Cust Feeder+Feedfund vs Fresh. COntoh: Taslimin/TASLIMIN
- Nama beda standar tulis, Phone beda, Komoditas & Leads ID salah satu null, Region beda tapi agak kurang meyakinkan. Kabupaten Sukabumi di Prov. Lampung? Di Lampung ada Sukabumi, tapi kecamatan, bukan kabupaten. Kabupaten Sukabumi harusnya di Jawa Barat (?). Feedfund+Feeder vs Fresh. Contoh: ASPAR/Aspar
- Triple: Nama beda, Region beda, Phone beda, Komoditas hanya satu terisi (udang vanname). Ini sepertinya memang pemiliknya sama tapi beda-beda daerah. Contoh: PPI - Farm Lampung, Banten, Yogyakarta. Contoh lain Pak Andre Jember vs Setyo Sutrisno.
- Nama beda, Phone beda, Region sama, Komoditas sama, Leads ID beda, BU sama. Contoh: Ujang Syarif Hidayat vs Salahudin
- Nama sama, Phone beda, Region beda, Komoditas irisan selisih satu (mas vs mas,nila), Leads ID beda, Cust Feeder vs Feedfund+Fresh. Contoh: Unang ID 3201170503670002
- Nama beda standar tulis, Region sama, Phone beda, Komoditas dan Leads ID salah satu null, Feedfund vs Fresh. Contoh: Ripai/RIPAI
- Nama, Region, Phone, Leads ID beda. Komoditas, BU sama (feeder). Contoh: Syarifudin vs Jonny Sinaga.
- Nama beda standar tulis, Phone beda, Region beda, Leads ID beda, Feeder vs Feedfund. Contoh: BAHRUL ULUM/Bahrul Ulum
- Nama beda, Phone sama, Region sama, Leads ID dan Komoditas salah satu null, Feedfund+Regmem vs Fresh. Contoh: 3201171506590015 Surya Mad Uhi vs Baban Subandi
- Nama beda dikit tulisan, Phone beda satu angka, Region beda, Leads beda, Komoditas irisan beda, Feedfund+Fresh vs Feeder. Contoh: Jami'ul Umam/Jamiul Umam 3201171506900044
- Triple: Nama beda dan beda dikit, Phone beda, Region ada beda ada sama, Leads ID beda, Feeder vs Feedfund vs Feeder. Komoditas mas vs mas,nila vs nila (mas,nila ada di Feedfund). Contoh 3201171802820002 Ade Nurul Hidayat vs DONI ADRIADI vs Doni Adriyadi
- Triple: Nama ada beda ada sama, Phone sama tapi ada kurang angka salah satunya, Region ada beda ada sama, Leads ID beda satunya null dan Komoditas sama satu null, Fresh vs Feeder+Feedfund vs Feeder. Contoh 3201171809620001 Ali Dani vs Deden vs Ali Dani.
- Nama beda standar tulis, Region sama, Leads ID dan Komoditas salah satu null, Phone sama. Feedfund vs Fresh. Contoh: Andi Imam Maulana/capslock semua
- Nama beda karena saltik, Phone sama, Leads ID dan Komoditas salah satu null, Region beda yang satu kota yang satu kabupaten tasikmalaya. Feeder vs Fresh. Contoh: 3206272204850002 Senif Apniyandi/Senif Apyandi.
- Triple: Nama ada beda ada mirip, Phone beda dan kurang angka, Region beda semua, Leads ID dan Komoditas beda, Feedfund vs Feedfund+Fresh vs Feeder. Contoh: DARYANTO vs Arie Sanjaya - eretan vs Arie sanjaya. 3212211408780002
- Nama sama, Phone kurang satu angka, Region sama, Komoditas irisan, Leads ID null satu, Feeder vs Feedfund+Regmem. Contoh: 3213080304640006 TATANG ANWAR
- Nama ada pemotongan, Phone beda, Region beda, Leads ID dan Komoditas beda, Feeder vs Feeder+Regmem. Contoh: 3318200507780002 Susawi Markus vs SUSAWI
- Nama beda standar tulis, Phone sama, Region beda(kota vs kab blitar), Komoditas sama, Leads ID satu null, Feeder vs Feedfund+Fresh. Contoh: 3505100401750003 M.rosuli vs M. ROSULI
- Nama beda standar tulis, Phone kurang satu angka, Region beda(kota vs kab blitar), Komoditas sama, Leads ID satu null, Feeder vs Feedfund+Regmem. Contoh: 3505102506760002 Amir Burhan vs Amir burhan.
- Nama beda, Phone beda, Region sama, Komoditas sama, Leads ID beda, BU sama. Contoh: Subandri vs Benni Ismanto
- Banyak: Nama beda, Phone beda, Region ada sama ada beda, Komoditas patin semua, Leads beda, Feeder semua. Contoh: 6308080810800003
- Biasanya yang Fresh yang kosong Leads ID, Komoditasnya.


_Catatan per 30 Juni 2020_



```python
!jupyter nbconvert --to HTML "/content/drive/My Drive/02 Engineering/Data Science/Notebooks/2020_06_25_Eksplorasi_Data_Cust_Lintas_BU.ipynb" --output "/content/My Drive/02 Engineering/Data Science/Notebooks/Eksplorasi_Data_Lintas_BU.html" --no-input --no-prompt
```
    [NbConvertApp] Converting notebook /content/drive/My Drive/02 Engineering/Data Science/Notebooks/2020_06_25_Eksplorasi_Data_Cust_Lintas_BU.ipynb to HTML
    [NbConvertApp] Writing 701485 bytes to /content/My Drive/02 Engineering/Data Science/Notebooks/Eksplorasi_Data_Lintas_BU.html



```python

```